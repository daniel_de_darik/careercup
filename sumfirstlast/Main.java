import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Random rnd;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt(), d = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();
        rnd = new Random();
        sort(0, n-1, a);
        System.out.println(Arrays.toString(a));
        for(int i=0;i<n-2;i++){
            int l = i + 1, r = n-1;
            while(l < r) {
                if (a[i] + a[l] + a[r] <= d) {
                    for(int j=l+1;j<=r;j++){
                        System.out.println(a[i] + " " + a[l] + " " + a[j]);    
                    }
                    l++;
                }else {
                    r--;
                }
            }
        }
        out.close();
    }

    private void sort(int l, int r, int []a) {
        int m = l + rnd.nextInt(r - l + 1);
        int x = a[m];
        int i = l, j = r;
        while(i <= j) {
            while(a[i] < x) i++;
            while(a[j] > x) j--;
            if (i <= j) {
                int t = a[i];
                a[i] = a[j];
                a[j] = t;
                i++;j--;
            }
        }

        if (l < j) sort(l, j, a);
        if (i < r) sort(i, r, a);
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}