import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	MyRandom rnd = new MyRandom(10);
	for(int i=0;i<25;i++){
	    System.out.println(rnd.nextInt());
	}
	
        out.close();
    }
    
    static class MyRandom{
	int []buffer;
	int pt, max, bufferSize;
	Random rnd;
	
	public MyRandom(int bufferSize){
	    this.bufferSize = bufferSize;
	    buffer = new int[bufferSize];
	    for(int i=0;i<bufferSize;i++){
		buffer[i] = max++;
	    }
	    pt = 0;
	    rnd = new Random();
	}
	
	public int nextInt(){
	    if (pt == bufferSize){
		for(int i=0;i<bufferSize;i++){
		    buffer[i] = max++;
		}
		pt = 0;
	    }
	    
	    int idx = pt + rnd.nextInt(bufferSize - pt);
	    int t = buffer[pt];
	    buffer[pt] = buffer[idx];
	    buffer[idx] = t;
	    
	    return buffer[pt++];
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}