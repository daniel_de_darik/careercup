import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        int l = -1, r = n - 1;
        while(r - l > 1) {
            int m = l + (r - l) / 2;
            if (a[m] >= a[r]) {
                l = m;
            }else {
                r = m;
            }
        }
        out.println(a[r] + " " + r);
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
