import java.util.*;

class gen {

	public void run() {
		Random rnd = new Random();
		int n = rnd.nextInt(4) + 1;
		System.out.println(n);
		int []a = new int[n];
		boolean used[] = new boolean[10001];
		for(int i = 0; i < n; ++i) {
			int x = rnd.nextInt(10000);
			while(used[x]) {
				x = rnd.nextInt(10000);
			}
			used[x] = true;
			a[i] = x;
		}
		Arrays.sort(a);
		int s = rnd.nextInt(n);
		for(int i = 0; i < n; ++i) {
			System.out.print(a[(s + i) % n] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		new gen().run();
	}
}