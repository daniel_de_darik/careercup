import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    private static Object lock1 = new Object();
    private static Object lock2 = new Object();
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        MyThread1 t1 = new MyThread1();
        MyThread2 t2 = new MyThread2();
        t1.start();
        t2.start();

        out.close();
    }

    private static class MyThread1 extends Thread {
        public void run() {
            synchronized(lock1) {
                System.out.println("Thread1: Holding lock1 ...");
                System.out.flush();
                try{
                    Thread.sleep(10);
                }catch(InterruptedException e){

                }
                System.out.println("Thread1: Waiting for lock2 ...");
                System.out.flush();
                synchronized(lock2) {
                    System.out.println("Thread: Holding lock1 & lock2 ...");
                    System.out.flush();
                }
            }
        }
    }

    private static class MyThread2 extends Thread {
        public void run() {
            synchronized(lock2) {
                System.out.println("Thread2: Holding lock2 ...");
                System.out.flush();
                try{
                    Thread.sleep(10);
                }catch(InterruptedException e){

                }
                System.out.println("Thread2: Waiting for lock2 ...");
                System.out.flush();
                synchronized(lock1) {
                    System.out.println("Thread2: Holding lock2 & lock1 ...");
                    System.out.flush();
                }
            }
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
