import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    Random rnd;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++){
	    a[i] = nextInt();
	}
	rnd = new Random();
	int x = kth(0, a.length-1, a, n/2);
	int c = 0;
	for(int i=0;i<a.length;i++) if (a[i] == x) c++;
	if(c >= n/2) out.println(x);
	else out.println("Not found");
        out.close();
    }
    
    private int kth(int l, int r, int []a, int k){
	int x = l + rnd.nextInt(r - l + 1);
	x = a[x];
	int i = l, j = r;
	while(i <= j){
	    while(a[i] < x) i++;
	    while(a[j] > x) j--;
	    if (i <= j){
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
		i++;j--;
	    }
	}
	
	if (l < k && k < j) return kth(l, j, a, k);
	if (i < k && k < r) return kth(i, r, a, k);
	
	return a[k];
    }

    public static void main(String args[]) throws Exception{
	new Solver().run();
    }
}