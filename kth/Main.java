import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt(), k = nextInt()-1;
        int []a = new int[n];
        for(int i=0;i<n;i++){
            a[i] = nextInt();
        }

        int x = kth(0, a.length-1, a, k);
        Arrays.sort(a);

        System.out.println(x + " " + a[k]);

        out.close();	
    }

    Random rnd = new Random();

    private int kth(int l, int r, int []a, int k) {
        int m = l + rnd.nextInt(r - l + 1);
        int x = a[m];
        int i = l, j = r;
        while(i <= j) {
            while(a[i] < x) i++;
            while(a[j] > x) j--;

            if (i <= j) {
                int t = a[i];
                a[i] = a[j];
                a[j] = t;
                i++;j--;
            }
        }

        if (l <= k && k <= j) return kth(l, j, a, k);
        if (i <= k && k <= r) return kth(i, r, a, k);

        return a[k];
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}