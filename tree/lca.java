import java.util.*;
import java.io.*;

public class lca{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Random rnd = new Random();

    int n;
    Node root;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        n = nextInt();

        Node []node = new Node[n];
        for(int i=0;i<n;i++){
            node[i] = new Node(i);
        }

        for(int i=1;i<n;i++){
            int x = nextInt();
            if (node[x].left == null) {
                node[x].left = node[i];
            }else {
                node[x].right = node[i];
            }
        }

        root = node[0];

        for(int i=0;i<n;i++){
            int x = rnd.nextInt(n);
            int y = rnd.nextInt(n);

            System.out.println(x + " " + y + " lca is " + lca(root, node[x], node[y]));
        }

        out.close();
    }

    private int count(Node root, Node v, Node u) {
        int ret = 0;
        if (root == null) return 0;
        if (root == v) ret++;
        if (root == u) ret++;
        if (ret == 2) return 2;
        ret+=count(root.left, v, u);
        if (ret == 2) return 2;

        return ret + count(root.right, v, u);
    }

    private Node lca(Node root, Node v, Node u) {
        if (u == v && root == v) return root;
        int left = count(root.left, v, u);
        if (left == 2) {
            return lca(root.left, v, u);
        }

        if (left == 1) {
            if (root == v) return root;
            if (root == u) return root;
        }

        int right = count(root.right, v, u);
        if (right == 2) {
            return lca(root.right, v, u);
        }
        if (right == 1) {
            if (root == v) return root;
            if (root == u) return root;
        }

        if (left + right == 2) {
            return root;
        }

        return null;
    }

    static class Node {
        public Node left, right;
        public int v;

        public Node(int v) {
            this.v = v;
        }

        public String toString() {
            return Integer.toString(v);
        }
    }

    public static void main(String args[]) throws Exception{
	   new lca().run();
    }
}