import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	int n = nextInt();
    	int []a = new int[n];
    	for(int i=0;i<n;i++){
    		a[i] = nextInt();
    	}
    	Arrays.sort(a);

    	Node root = build(0, n-1, a);
    	print(root);

        out.close();	
    }

    private Node build(int left, int right, int []a) {
    	// System.out.println(left + " " + right);
    	if (right < left) return null;
    	if (left == right) 
    		return new Node(a[left]);
    	
    	int mid = (left + right)/2;
    	Node node = new Node(a[mid]);
    	node.left = build(left, mid - 1, a);
    	node.right = build(mid + 1, right, a);

    	return node;
    }

    private void print(Node node) {
    	if (node == null) return;
    	print(node.left);
    	System.out.println(node.data);
    	print(node.right);
    }

    static class Node {
    	int data;
    	Node left, right;
    	public Node(int x) {
    		this.data = x;
    		left = null;
    		right = null;
    	}

    	public void setLeft(Node left) {
    		this.left = left;
    	}

    	public void setRight(Node right) {
    		this.right = right;
    	}
    }

    public static void main(String args[]) throws Exception{
	   new Solver().run();
    }
}