import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    List<Integer> g[];
    int n;
    boolean []used;
    int []level;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        n = nextInt();

        g = new LinkedList[n];
        for(int i=0;i<n;i++)
            g[i] = new LinkedList<Integer>();
        
        int m = nextInt();
        for(int i=0;i<m;i++){
            int a = nextInt();
            int b = nextInt();
            g[a].add(b);
            g[b].add(a);
        }

        used = new boolean[n];
        level = new int[n];

        dfs(0, -1);

        int max = Integer.MIN_VALUE/2;
        int min = Integer.MAX_VALUE/2;
        for(int i=0;i<n;i++){
            if (g[i].size() == 1) {
                max = Math.max(max, level[i]);
                min = Math.min(min, level[i]);
            }
        }

        System.out.println(Arrays.toString(level));
        System.out.println(max + " " + min);
        if (max - min > 1) {
            System.out.println("Not balanced");
        }else {
            System.out.println("balanced");
        }

        out.close();
    }

    private void dfs(int x, int parent) {
        used[x] = true;
        if (parent != -1)
            level[x] = level[parent] + 1;
        
        for(int i : g[x]) {
            if (!used[i]) {
                dfs(i, x);
            }
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}