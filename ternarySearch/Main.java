import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	int x = ternarySearch(a);
	out.println(a[x-1] + " " + a[x] + " " + a[x+1]);
        out.close();
    }
    
    private int ternarySearch(int []a){
	int n = a.length;
	int l = -1, r = n;
	while(r - l > 2){
	    int m1 = l + (r - l)/3;
	    int m2 = r - (r - l)/3;
	    if (a[m1] > a[m2]) l = m1;
	    else r = m2;
	}
	
	int idx = l;
	for(int i=l;i<=r;i++)
	    if (a[idx] > a[i]) idx = i;
	
	return idx;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}