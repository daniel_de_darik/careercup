import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    Random rnd;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	rnd = new Random();
	
	Map<Integer, Integer> map = new HashMap<Integer, Integer>();
	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++){
	    a[i] = nextInt();
	}
	
	for(int i=0;i<n;i++){
	    if (!map.containsKey(a[i])){
		map.put(a[i], 0);
	    }
	    map.put(a[i], map.get(a[i]) + 1);
	}
	
	if (map.size() < 3){
	    System.out.println("No solution");
	    return;
	}
	
	int c = 0;
	int pos = 0;
	while(c++ < 3){
	    int x = kth(0, n-1, a, pos);
	    out.println(x);
	    int offset = map.get(x);
	    pos+=offset;
	}
	
        out.close();
    }
    
    private int kth(int l, int r, int []a, int k){
	int m = l + rnd.nextInt(r - l + 1);
	int x = a[m];
	int i = l, j = r;
	while(i <= j){
	    while(a[i] < x) i++;
	    while(a[j] > x) j--;
	    
	    if (i <= j){
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
		i++;
		j--;
	    }
	}
	
	if (l <= k && k <= j) return kth(l, j, a, k);
	if (i <= k && k <= r) return kth(i, r, a, k);
	
	return a[k];
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}