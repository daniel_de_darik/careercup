import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	Queue<Integer> q = new ArrayDeque<Integer>();
	int n = nextInt();
	for(int i=0;i<n;i++) q.add(nextInt());
	while(q.size() > 0) out.print(q.poll() + " ");
	out.println();
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Solver().run();
    }
}