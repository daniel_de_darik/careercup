import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	MyQueue<Integer> q = new MyQueue<Integer>();
	int n = nextInt();
	for(int i=0;i<n;i++) q.push(nextInt());
	while(q.size() > 0) out.print(q.pop() + " ");
	out.println();
        out.close();
    }
    
    class MyQueue<T>{
	Stack<T> st1, st2;
	
	public MyQueue(){
	    st1 = new Stack<T>();
	    st2 = new Stack<T>();
	}
	
	public int size(){
	    return st1.size() + st2.size();
	}
	
	public void push(T elm){
	    st1.push(elm);
	}
	
	public T peek() throws Exception{
	    if (size() == 0) throw new Exception("No elements in stack exception");
	    if (st2.size() == 0) move();
	    return st2.peek();
	}
	
	public T pop() throws Exception{
	    if (size() == 0) throw new Exception("No elements in stack exception");
	    if (st2.size() == 0) move();
	    return st2.pop();
	}
	
	private void move(){
	    while(st1.size() > 0) st2.push(st1.pop());
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}