import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Random rnd = new Random();
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	HashFunction myHf = new HashFunction(){
	    public int hash(String s){
		int p = 257, hash = 0;
		for(int i = 0; i < s.length(); ++i){
		    hash = hash * p + s.charAt(i);
		}
		return hash;
	    }
	};
	
	CircleHash ch = new CircleHash(myHf, 10);    
	List<Node> lst = new LinkedList<>();
	for(int i = 0; i < 5; ++i){
	    lst.add(new Node(rnd.nextInt(10000)));
	}
	for(Node i : lst){
	    ch.addNode(i);
	}
	
	String []a = new String[5];
	for(int i = 0; i < 5; ++i) a[i] = getString();
	for(String i : a)
	    System.out.println(ch.get(i));	
	System.out.println();
	for(int i = 0; i < 3; ++i) ch.removeNode(lst.get(i));
	for(String i : a)
	    System.out.println(ch.get(i));
        out.close();
    }

    private String getString(){
	String alph = "abcdefghijklmnopqrstuvwxyz";
	String ret = "";
	while(ret.length() < 100){
	    ret += alph.charAt(rnd.nextInt(alph.length()));
	}
	return ret;
    }
    
    class Node {
	int id;
	public Node(int id){
	    this.id = id;
	}
	
	public String toString(){
	    return "" + id;
	}
    }
    
    class CircleHash {
	final int numberOfReplicas;
	final HashFunction hf;
	
	TreeMap<Integer, Node> map;
	
	public CircleHash(HashFunction hf, int numberOfReplicas){
	    this.hf = hf;
	    this.numberOfReplicas = numberOfReplicas;
	    map = new TreeMap<Integer, Node>();
	}
	
	public void addNode(Node node){
	    for(int i = 0; i < numberOfReplicas; ++i){
		map.put(hf.hash(node + "" + i), node);
	    }
	}
	
	public void removeNode(Node node){
	    for(int i = 0; i < numberOfReplicas; ++i){
		map.remove(hf.hash(node + "" + i));
	    }
	}
	
	public Node get(String obj){
	    //for(int i : map.keySet()) System.out.println(i);
	    if (map.size() == 0) return null;
	    int hash = hf.hash(obj);
	    SortedMap<Integer, Node> tailMap = map.tailMap(hash);
	    int key = -1;
	    if (tailMap.size() > 0) key = tailMap.firstKey();
	    else key = map.firstKey();
	    
	    return map.get(key);
	}
    }
    
    interface HashFunction {
	public int hash(String s);
    }
    
    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}