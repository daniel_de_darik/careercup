import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

	C c = new C(1);

        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}

class A{
    public A(){
	System.out.println("A");
    }
}

class D{
    /*
    public D(){
	System.out.println("D");
	hello();
    }
    */
    protected void hello(){
	System.out.println("hello");
    }
}

class B extends D{
    public B(){
	System.out.println("B");
    }
}

class C extends B{
    private A a = new A();
    
    public C(){
	//super();
	System.out.println("C");
    }
    
    public C(int x){
	System.out.println(x);
    }
}