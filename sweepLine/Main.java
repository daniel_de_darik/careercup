import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
    
	int n = nextInt();
	Edge []e = new Edge[2*n];
	for(int i=0;i<n;i++){
	    int x = nextInt(), y = nextInt();
	    int x1 = nextInt(), y1 = nextInt();
	    
	    e[2*i] = new Edge(x, y1, y, false);
	    e[2*i+1] = new Edge(x1, y1, y, true);
	}
	
	Arrays.sort(e, 0, e.length, new Cmp());
	/*
	for(int i=0;i<e.length;i++){
	    out.println(e[i]);
	}
	*/
	int area = 0;
	int []line = new int[10001];
	for(int i=0;i<e.length;i++){
	    int length = 0;
	    for(int j=0;j<line.length;j++){
		if (line[j] > 0) length++;
	    }
	    if (i > 0){
		area+=length * (e[i].x - e[i-1].x);
	    }
	    
	    int j = i;
	    while(j < 2*n && e[i].x == e[j].x){
		j++;
	    }
	    for(int k=i;k<j;k++){
		if (e[k].isEnd){
		    for(int l=e[k].y;l<e[k].y1;l++){
			line[l]--;
		    }
		}else{
		    for(int l=e[k].y;l<e[k].y1;l++){
			line[l]++;
		    }
		}
	    }
	    i = j - 1;
	}
	
	out.println(area);
        out.close();
    }

    static class Edge{
	public int x, y, y1;
	public boolean isEnd;
	
	public Edge(int x, int y, int y1, boolean isEnd){
	    this.x = x;
	    this.y = y;
	    this.y1 = y1;
	    this.isEnd = isEnd;
	}
	
	public String toString(){
	    return this.x + " " + this.y + " " + this.y1 + " " + this.isEnd;
	}
    }
    
    static class Cmp implements Comparator<Edge>{
	public int compare(Edge a, Edge b){
	    if (a.x > b.x) return 1;
	    if (a.x < b.x) return -1;
	    if (a.isEnd && b.isEnd){
		if (a.y > b.y) return 1;
		if (a.y < b.y) return -1;
		return 0;
	    }
	    if (a.isEnd) return -1;
	    if (b.isEnd) return 1;
	    if (a.y > b.y) return 1;
	    if (a.y < b.y) return -1;
	    return 0;
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}