import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        out.close();
    }

    Map<Integer, Integer> map;
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0) return null;
        map = new HashMap<Integer, Integer>();
        for(int i = 0; i < inorder.length; ++i){
            map.put(inorder[i], i);
        }
        return build(0, preorder.length - 1, 0, preorder.length-1, preorder, inorder);
    }
    
    private TreeNode build(int l1, int r1, int l2, int r2, int[] preorder, int[] inorder){
        if (l1 > r1) return null;
        TreeNode root = new TreeNode(preorder[l1]);
        int x = map.get(preorder[l1]);
        root.left = build(l1 + 1, l1 + x - l2, l2, x - 1, preorder, inorder);
        root.right = build(l1 + x - l2 + 1, r1, x + 1, r2, preorder, inorder);
        return root;
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
