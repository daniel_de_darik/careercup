import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    int n;
    int total;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	total = 1;
	for(int i=0;i<n;i++) total*=a[i];
	rec(0, 1, a);
        out.close();
    }
    
    private int rec(int i, int prefix, int[]a){
	if (i == n) return 1;
	int suffix = rec(i+1, prefix * a[i], a);
	out.println(i + " " + prefix * suffix);
	return suffix * a[i];
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}