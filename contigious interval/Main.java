import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    int n;
    List<Integer> []g;
    boolean used[];
    int cmp, minV, maxV;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	n = nextInt();
	g = new LinkedList[n+1];
	for(int i=1;i<=n;i++) g[i] = new LinkedList<Integer>();
	HashSet<Integer> set = new HashSet<Integer>();
	for(int i=0;i<n;i++){
	    set.add(nextInt());
	}
	
	for(int i : set){
	    if (set.contains(i-1)){
		g[i].add(i-1);
	    }
	    if (set.contains(i+1)){
		g[i].add(i+1);
	    }
	}
	
	int ret = 0, u = -1, v = -1;
	used = new boolean[n+1];
	for(int i=1;i<=n;i++){
	    if (!used[i]){
		cmp = 0;
		minV = -1;
		maxV = -1;
		dfs(i, -1);
		if (cmp > ret){
		    ret = cmp;
		    u = minV;
		    v = maxV;
		}
	    }
	}
	
	out.println(ret);
	out.println(u + " " + v);
        out.close();	
    }
    
    private void dfs(int x, int p){
	used[x] = true;
	cmp++;
	if (minV == -1 || minV > x) minV = x;
	if (maxV == -1 || maxV < x) maxV = x;
	for(int i : g[x]){
	    if (i == p) continue;
	    dfs(i, x);
	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}