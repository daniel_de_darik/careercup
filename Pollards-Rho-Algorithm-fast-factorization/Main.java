//fast factorization works approximately N ^ (1/4)
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }

    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    private long nextLong() throws Exception {
        return Long.parseLong(next());
    }

    Random rnd = new Random();
    List<Long> fact;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        long N = nextLong();
        fact = new LinkedList<>();
        factor(N);
        Collections.sort(fact);
        for(long i : fact)
            out.println(i);
        out.close();
    }

    private void factor(long N) {
        if (N == 1) return;
        if (isPrime(N)) {
            fact.add(N);
            return;
        }
        int it = 0;
        long div = divisor(N);
        while(it++ < 10 && div == 1) div = divisor(N);
        if (div > 1) {
            factor(div);
            factor(N / div);
        }else {
            fact.add(N);
        }
    }

    private long divisor(long N) {
        if (N % 2 == 0) return 2;
        long a = rnd.nextInt(), b = rnd.nextInt(), c = rnd.nextInt();
        int it = 0;
        while(it++ < 100) {
            a = f(a, c, N);
            b = f(f(b, c, N), c, N);

            long gcd = gcd(N, Math.abs(a - b));
            if (gcd > 1) return gcd;
        }
        return 1;
    }

    private long f(long a, long c, long N) {
        return ((a * a) % N + c) % N;
    }

    private long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    private boolean isPrime(long n) {
        for(int i = 2; 1L * i * i <= n; ++i) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}