import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	/*
	Node head = new Node(4);
	
	Node three = new Node(3);
	three.setNested(new Node(1));
	three.setNext(new Node(2));
	head.setNested(three);
	
	Node five = new Node(5);
	Node seven = new Node(7);
	Node six = new Node(6);
	seven.setNext(six);
	five.setNext(seven);
	head.setNext(five);
	*/
	
	Node head = new Node(1);
	Node two = new Node(2);
	Node three = new Node(3);
	three.setNested(new Node(4));
	Node five = new Node(5);
	five.setNext(new Node(6));
	three.setNext(five);
	two.setNext(three);
	head.setNext(two);
	
	MyIterator it = new MyIterator(head);
	while(it.hasNext()){
	    System.out.println(it.next());
	}
	
        out.close();	
    }
    
    static class Node{
	Node nested, next;
	int x;
	
	public Node(int x){
	    this.x = x;
	}
	
	public void setNested(Node node){
	    this.nested = node;
	}
	
	public void setNext(Node node){
	    this.next = node;
	}
    }
    
    static class MyIterator{
	Stack<Node> stack = new Stack<Node>();
	
	public MyIterator(Node head){
	    stack.add(head);
	}
	
	public boolean hasNext(){
	    return stack.size() > 0;
	}
	
	public int next(){
	    Node cur = stack.pop();
	    int ret = cur.x;
	    if (cur.nested != null){
		stack.push(cur);
		stack.push(cur.nested);
	    }else{
		cur = cur.next;
		while(cur == null && stack.size() > 0){
		    cur = stack.pop();
		    if (cur.next != null){
			cur = cur.next;
			break;
		    }
		}
		if (cur != null) stack.push(cur);
	    }
	    
	    return ret;
	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}