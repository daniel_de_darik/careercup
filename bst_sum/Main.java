import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    Node root;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	add(8);
	add(5);
	add(2);
	add(6);
	add(1);
	add(1);
	add(4);
	add(9);
	add(3);
	add(4);
	add(3);
	add(-1);
	add(6);
	
	print(root);
	System.out.println();
	
	find(root, 7);
	
        out.close();
    }
    
    private void print(Node cur){
	if (cur == null) return;
	print(cur.left);
	System.out.print(cur.key + " ");
	print(cur.right);
    }
    
    private void find(Node root, int x){
	Stack<Node> stack1 = new Stack<Node>();
	Stack<Node> stack2 = new Stack<Node>();
	
	Node cur = root;
	while(cur != null){
	    stack2.push(cur);
	    cur = cur.right;
	}
	
	cur = root.left;
	while(cur != null){
	    stack1.push(cur);
	    cur = cur.left;
	}
	
	while(stack1.size() > 0 && stack2.size() > 0 && stack1.peek().key < stack2.peek().key){
	    Node left = stack1.peek();
	    Node right = stack2.peek();
	    if (left.key + right.key > x){
		right = stack2.pop();
		if (right.left != null){
		    cur = right.left;
		    while(cur != null){
			stack2.push(cur);
			cur = cur.right;
		    }
		}
	    }else if (left.key + right.key < x){
		left = stack1.pop();
		if (left.right != null){
		    cur = left.right;
		    while(cur != null){
			stack1.push(cur);
			cur = cur.left;
		    }
		}
	    }else{
		System.out.println(left + " " + right);
		stack1.pop();
		stack2.pop();
		if (right.left != null){
		    cur = right.left;
		    while(cur != null){
			stack2.push(cur);
			cur = cur.right;
		    }
		}
		if (left.right != null){
		    cur = left.right;
		    while(cur != null){
			stack1.push(cur);
			cur = cur.left;
		    }
		}
	    }
	}
    }
    
    private void add(int key){
	Node node = new Node(key);
	if (root == null){
	    root = node;
	    return;
	}
	
	Node cur = root, parent = null;
	boolean isLeft = false;
	while(true){
	    parent = cur;
	    if (cur.key < key){
		isLeft = false;
		cur = cur.right;
	    }else{
		isLeft = true;
		cur = cur.left;
	    }
	    if (cur == null){
		if (isLeft) parent.left = node;
		else parent.right = node;
		break;
	    }
	}
    }

    static class Node{
	int key;
	Node left, right;
	public Node(int key){
	    this.key = key;
	}
	
	@Override
	public String toString(){
	    return Integer.toString(key);
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}