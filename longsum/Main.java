import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        char []a = next().toCharArray();
        char []b = next().toCharArray();

        Character []c = calc(a, b);

        for(Character i : c) {
            out.print(i);
        }
        out.println();
        out.close();	
    }

    private Character[] calc(char []a, char []b){
      a = reverse(a);
      b = reverse(b);
      int n  = a.length, m = b.length;
      if (n > m){
        int t = n;
        n = m;
        m = t;

        char tmp[] = a;
        a = b;
        b = tmp;
      }
      List<Character> r = new ArrayList<Character>();
      int carry = 0;
      for(int i=0;i<n;i++){
        int s = a[i] - '0' + b[i] - '0' + carry;
        carry = 0;
        if (s >= 10) {
          s-=10;
          carry = 1;
        }
        r.add((char)(s + '0'));
      }

      for(int i=n;i<m;i++){
        int s = b[i] - '0' + carry;
        carry = 0;
        if (s >= 10) {
          s-=10;
          carry = 1;
        }
        r.add((char)(s + '0'));
      }

      if (carry > 0) r.add((char)(carry + '0'));
      Collections.reverse(r);
      
      return r.toArray(new Character[0]);
    }

    private char []reverse(char []a){
      for(int i=0,j=a.length-1;i<j;i++,j--){
        char c = a[i];
        a[i] = a[j];
        a[j] = c;
      }
      return a;
    }



    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}