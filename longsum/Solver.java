public class Solver {

	public void run() {
		MyStack<Integer> stack = new MyStack<Integer>();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);

		while(stack.size() > 0){
			System.out.println(stack.peek());
			System.out.println(stack.pop());
		}
	}

	public static void main(String[] args) {
		new Solver().run();
	}
}

class MyStack<T>{
  class Node<T>{
    T data;
    Node<T> next;
    
    public Node(T data){
      this.data = data;
      this.next = null;
    }

    public void setNext(Node<T> next){
      this.next = next;
    }

    public Node<T> getNext(){
      return this.next;
    }
  }

  Node<T> top;
  int size;
  
  public MyStack(){
    size = 0;
    top = null;
  }

  public int size(){
    return this.size;
  }

  public T peek(){
    if (size() == 0) return null;
    return top.data;
  }

  public void push(T data){
     Node<T> elm = new Node<T>(data);
     if(size() > 0) elm.setNext(top);
     top = elm;
     size++;
  }

  public T pop(){
    if (size() == 0) return null;
    Node<T> ret = top;
    top = top.getNext();
    size--;

    return ret.data;
  }
}
