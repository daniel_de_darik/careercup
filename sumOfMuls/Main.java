import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	int []prefix = new int[n];
	int []sum = new int[n];
	sum[0] = a[0] * a[0];
	prefix[0] = a[0];
	
	for(int i=1;i<n;i++){
	    sum[i] = sum[i-1] + a[i] * a[i];
	    prefix[i] = prefix[i-1] + a[i];
	}
	
	SegmentTree tree = new SegmentTree(a);
	int m = nextInt();
	for(int i=0;i<m;i++){
	    int l = nextInt(), r = nextInt();
	    Node info = tree.sum(1, 0, n-1, l, r);
	    out.print(info.val + " ");
	    int ans = prefix[r];
	    if (l > 0) ans-=prefix[l-1];
	    ans*=ans;
	    int x = sum[r];
	    if (l > 0) x-=sum[l-1];
	    
	    out.println((ans - x)/2);
	}
	
        out.close();
    }
    
    static class Node{
	int sum, val;
	public Node(int sum, int val){
	    this.sum = sum;
	    this.val = val;
	}
    }
    
    static class SegmentTree{
	Node []tree;
	int n;
	public SegmentTree(int []a){
	    n = a.length;
	    tree = new Node[4*n];
	    build(1, 0, n-1, a);
	}
	
	private void build(int v, int tl, int tr, int []a){
	    if (tl == tr){
		tree[v] = new Node(a[tl], 0);
		return;
	    }
	    
	    int tm = (tl + tr) >> 1;
	    build(2*v, tl, tm, a);
	    build(2*v+1, tm+1, tr, a);
	    
	    tree[v] = merge(tree[2*v], tree[2*v+1]);
	}
	
	public Node sum(int v, int tl, int tr, int l, int r){
	    if (l > r) return new Node(0, 0);
	    if (tl == l && tr == r){
		return tree[v];
	    }
	    int tm = (tl + tr) >> 1;
	    Node left = sum(2*v, tl, tm, l, Math.min(tm, r));
	    Node right = sum(2*v+1, tm + 1, tr, Math.max(l, tm + 1), r);
	    
	    return merge(left, right);
	}
	
	private Node merge(Node left, Node right){
	    Node n = new Node(0, 0);
	    n.sum = left.sum + right.sum;
	    n.val = left.val + right.val + left.sum * right.sum;
	    
	    return n;
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}