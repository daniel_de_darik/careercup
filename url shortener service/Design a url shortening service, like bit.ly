Design a url shortening service, like bit.ly

Use Cases

1. Shortening: take a url => return much shorter url
2. Redirection: take a short url => redirect to the original
3. Custom url
4. High availability of the system

Try to find more use cases, discuss with interviewer

4. Analytics
5. Automatic link expiration
6. Manual link removal
7. UI vs API

----------------------------------------------

Problem Constraints (Math)
- Amount of traffic system should handle
- Amount of data system should handle

Say 15 * (10 ^ 9) new tweets, say mostly it used by twitter

All shortened URLs per month: 1.5 * 10 ^ 9
Sites below top 3: shorted 300 * (10 ^ 6) per month
1. We: 100 * 10 ^ 6 urls each month
2. 10^9 requests per month
3. 10% from shortening and 90% from redirection

convert this to request per sec
4. Requests per second: 400+ = (10^9 / 24 / 60 / 60) (40: shortens, 360: redirects, because of 3)
data storage
5. Total urls: 5 years X 12 month X 100 * 10 ^ 6 = 6 * 10 ^ 9 urls in 5 years
6. 500 bytes per URL
7. Alphabet a-zA-z0-9 = log base 62 => 6 bytes per short url (hash)
8. 3TB for all urls, 36GB for all hashes (over 5 years)
9. New data written per second: 40 X (500 + 6): 20K
10. Data read per second: 360 X (500 + 6): 180K

----------------------------------------------------

Abstract design

1. Application service layer (serves the requests)
 a. Shortening service
   Generate hash, check if in store, store new mapping.
   hashed_url = convert_to_base62(md5(original_url + random_salt))[:6]
 b. Redrirection service
2. Data storage layer (keeps track of the hash -> url mappings)
 a. Acts like a big hash table: stotres new mappings, and retrieves mappings given key

 ------------------------------------------------------

 Understanding bottlenecks

 Our application is lightweight
 1. Redirect: Call to the data store + redirect
 2. Call to the data store + generate hash (40 hash per sec is OK)

 Data problem is more interesting:
 We can't afford 3TB lookup per redirect

 Bottlenecks:
 Traffic is probably not going to very hard, data - more interesting

