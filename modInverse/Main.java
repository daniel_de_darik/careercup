import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt(), m = nextInt();
	if (!hasSolution(n, m)) {
	    out.println("No inverse");
	    out.close();
	    return;
	}
	System.out.println(modInverse(n, m));
        out.close();
    }
    
    private int modInverse(int a, int b){
	Pair p = gcdExtended(a, b);
	int inv = pow(a, phi(b) - 1, b);
	if (p.x < 0) p.x += b;
	System.out.println(inv + " " + p.x);
	return p.x;
    }
    
    private Pair gcdExtended(int a, int b){
	if (b == 0){
	    // a * x + b * y = 1
    	    // here b = 0
    	    // hence a * x = 1
    	    // if a is not 1, then error else x = 1 and y = 0
    	    // Note that error wont be here, we will always find a which is not 1
    	    // as error case is already handle in solveThis function
	    return new Pair(1, 0);
	}else{
	    Pair p = gcdExtended(b, a % b);
	    int x1 = p.x;
	    int y1 = p.y;
	    
	    //x = y1;
	    //y = x1 - a / b * y1;
	    
	    return new Pair(y1, x1 - (a / b) * y1);
	}
    }
    
    private int pow(int x, int p, int MOD){
	int ret = 1;
	while(p > 0){
	    if (p % 2 == 1) ret = (ret * x) % MOD;
	    x = (x * x) % MOD;
	    p >>= 1;
	}
	return ret;
    }
    
    private int phi(int n){
	int res = n;
	for(int i = 2; i * i <= n; ++i){
	    if (n % i == 0) res -= res / i;
	    while(n % i == 0) n /= i;
	}
	if (n > 1) res -= res / n;
	return res;
    }
    
    private boolean hasSolution(int a, int b){
	int gcd = gcd(a, b);
	if (gcd != 1) return false;
	return true;
    }
    
    private int gcd(int a, int b){
	return b == 0 ? a : gcd(b, a % b);
    }
    
    class Pair {
	int x, y;
	public Pair(int x, int y){
	    this.x = x;
	    this.y = y;
	}
	
	public String toString(){
	    return this.x + " " + this.y;
	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}