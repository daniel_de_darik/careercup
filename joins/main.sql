use joins_tutorial;
insert into tableA (name) values('Pirate'), ('Monkey'), ('Ninja'), ('Spaghetti');
insert into tableB (name) values('Rutabaga'), ('Pirate'), ('Darth Vader'), ('Ninja');

-- inner join
select * from tableA
inner join tableB on tableA.name = tableB.name

-- outer join
select * from tableA
left join tableB on tableA.name = tableB.name
union
select * from tableA
right join tableB on tableA.name = tableB.name

-- union with no intersection
select * from tableA
left join tableB on tableA.name = tableB.name
where tableB.id is NULL
union
select * from tableA
right join tableB on tableA.name = tableB.name
where tableA.id is NULL

-- left join
select * from tableA
left join tableB on tableA.name = tableB.name

-- left join result only in A but not in B
select * from tableA
left join tableB on tableA.name = tableB.name
where tableB.id is NULL

-- right join
select * from tableA 
right join tableB on tableA.name = tableB.name

-- right join results in B but not in A
select * from tableA
right join tableB on tableA.name = tableB.name
where tableA.id is NULL