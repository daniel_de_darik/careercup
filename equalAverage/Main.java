import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        if (n % 2 == 1) {
            System.out.println("NO");
            return;
        }
        int []a = new int[n+1];
        int sum = 0;
        for(int i=1;i<=n;i++) {
            a[i] = nextInt();
            sum+=a[i];
        }
        if (sum % 2 == 1) {
            System.out.println("NO");
            return;
        }
        int prevj[][][] = new int[n+1][n+1][sum+1];
        int prevs[][][] = new int[n+1][n+1][sum+1];
        boolean dp[][][] = new boolean[n+1][n+1][sum+1];
        dp[0][0][0] = true;
        for(int i=1;i<=n;i++){
            for(int j=0;j<=i;j++){
                for(int s=0;s<=sum;s++){
                    dp[i][j][s] = dp[i-1][j][s];
                    if (dp[i-1][j][s]) {
                        prevj[i][j][s] = j;
                        prevs[i][j][s] = s;
                    }
                    if (s - a[j] >= 0) {
                        if (dp[i-1][j-1][s-a[j]]) {
                            dp[i][j][s]|=dp[i-1][j-1][s-a[j]];
                            prevj[i][j][s] = j-1;
                            prevs[i][j][s] = s - a[j];
                        }
                    }
                }
            }
        }

        for(int i=0;i<=n;i++){
            if (dp[n][i][sum/2]) {
                System.out.println("YES");
                return;
            }
        }
        System.out.println("NO");
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}