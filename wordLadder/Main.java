/*
Given a string s and a dictionary of words dict, add spaces in s to construct a sentence where each word is a valid dictionary word.

Return all such possible sentences.

For example, given
s = "catsanddog",
dict = ["cat", "cats", "and", "sand", "dog"].

A solution is ["cats and dog", "cat sand dog"].
*/
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        out.close();
    }

    List<String> ret;
    String s;
    Set<String> dict;
    boolean []dp;
    List<P> set;
    
    final long PRIME = 31;
    long []p, hash;
    
    public List<String> wordBreak(String s, Set<String> dict) {
        this.s = s;
        this.dict = dict;
        int n = s.length();
        
        p = new long[n];
        p[0] = 1;
        for(int i = 1; i < n; ++i) p[i] = p[i-1] * PRIME;
        
        set = new LinkedList<>();
        for(String i : dict){
            if (i.length() > s.length()) continue;
            long hh = 0;
            for(int j = 0; j < i.length(); ++j){
                hh += p[j] * (i.charAt(j) - 'a' + 1);
            }
            set.add(new P(hh, i));
        }
        
        hash = new long[n];
        for(int i = 0; i < s.length(); ++i){
            hash[i] = (s.charAt(i) - 'a' + 1) * p[i];
            if (i > 0) hash[i] += hash[i-1];
        }
        
        dp = new boolean[n + 1];
        dp[0] = true;
        for(int i = 1; i <= n; ++i){
            for(P j : set){
                int start = i - j.s.length();
                if (start >= 0 && dp[start]){
                    long h1 = hash[i-1];
                    if (start - 1 >= 0) h1 -= hash[start - 1];
                    if (h1 == j.hash * p[(i-1) - (j.s.length() - 1)]) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }
        ret = new LinkedList<>();
        if (!dp[n]) return ret;
        backtrack(n, "");
        return ret;
    }
    
    private void backtrack(int n, String str){
        if (n == 0){
            ret.add(str.substring(0, str.length() - 1));
            return;
        }
        for(P j : set){
            int start = n - j.s.length();
            if (start >= 0 && dp[start]){
                long h1 = hash[n - 1];
                if (start - 1 >= 0) h1 -= hash[start - 1];
                if (h1 == j.hash * p[(n - 1) - (j.s.length() - 1)]) {
                    backtrack(n - j.s.length(), j.s + " " + str);
                }
            }
        }
    }
    
    static class P{
        long hash;
        String s;
        public P(long hash, String s){
            this.hash = hash;
            this.s = s;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}