#!/usr/bin/python
class P:
	def __init__(self, hash, s):
		self.hash = hash
		self.s = s

class Solution:
	PRIME = 31
	def wordBreak(self, s, words):
		n = len(s)
		self.p = [1 for _ in xrange(n)]
		for i in xrange(1, n):
			self.p[i] = self.p[i-1] * Solution.PRIME

		self.hh = [0 for _ in xrange(n)]
		for i in xrange(n):
			self.hh[i] = (ord(s[i]) - ord('a') + 1) * self.p[i]
			if i > 0:
				self.hh[i] += self.hh[i-1]

		self.lst = []
		for i in words:
			if len(i) > len(s):
				continue
			h = 0
			for j in xrange(len(i)):
				h += self.p[j] * (ord(i[j]) - ord('a') + 1)
			self.lst.append(P(h, i))

		self.dp = [False for _ in xrange(n + 1)]
		self.dp[0] = True
		for i in xrange(1, n + 1):
			for j in self.lst:
				start = i - len(j.s)
				if start < 0:
					continue
				if self.dp[start]:
					h1 = self.hh[i-1]
					if start - 1 >= 0:
						h1 -= self.hh[start - 1]
					if j.hash * self.p[i - 1 - (len(j.s) - 1)] == h1:
						self.dp[i] = True;
						break

		if not self.dp[n]:
			return []

		self.ret = []
		self.backtrack(n, "")

		return self.ret

	def backtrack(self, n, s):
		if n == 0:
			self.ret.append(s[:-1])
			return

		for j in self.lst:
			start = n - len(j.s)
			if start < 0:
				continue
			if self.dp[start]:
				h1 = self.hh[n - 1]
				if start - 1 >= 0:
					h1 -= self.hh[start - 1]
				if j.hash * self.p[n - 1 - (len(j.s) - 1)]	== h1:
					self.backtrack(n - len(j.s), j.s + " " + s)

s, n = raw_input(), int(raw_input())
words = set()
for _ in xrange(n):
	words.add(raw_input())

solver = Solution()
ret = solver.wordBreak(s, words)
print ret
