import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        char []a = next().toCharArray();
        Arrays.sort(a);

        out.println(Arrays.toString(a));

        // String s1 = next();
        // String s2 = next();

        // System.out.println(isAnagram(s1, s2));

        // int n = s.length();
        // char []a = s.toCharArray();
        // Comparator<Character> comp = new Comparator<Character>() {
        //     public int compare(Character a, Character b) {
        //         return a.charValue() - b.charValue();
        //     }
        // };
        // Arrays.sort(a, comp);
        // Arrays.sort(a);
        // int yk = 1;
        // for(int i=1;i<n;i++){
        //     if (a[i] != a[i-1]) {
        //         a[yk++] = a[i];
        //     }
        // }

        // System.out.println(new String(a, 0, yk));

        // char []a = in.readLine().toCharArray();
        // int n = a.length;
        // int space = 0;
        // for(int i=0;i<n;i++){
        //     if (a[i] == ' ')
        //         space++;
        // }

        // char []b = new char[n - space + 3 * space];
        // int yk = 0;
        // for(int i=0;i<n;i++){
        //     if (a[i] == ' ') {
        //         b[yk++] = '%';
        //         b[yk++] = '2';
        //         b[yk++] = '0';
        //     }else {
        //         b[yk++] = a[i];
        //     }
        // }

        // out.println(new String(b));

        // int n = nextInt();
        // char [][]a = new char[n][];
        // for(int i=0;i<n;i++){
        //     a[i] = next().toCharArray();
        // }

        // for(int i=0;i<n;i++){
        //     for(int j=0;j<n;j++){
        //         System.out.print(a[i][j]);
        //     }
        //     System.out.println();
        // }

        // System.out.println("-----------------------");

        // for(int layer=0;layer<n/2;layer++){
        //     for(int i=layer;i<n-layer-1;i++){
        //         char tmp = a[layer][i];
        //         a[layer][i] = a[n-i-1][layer];
        //         a[n-i-1][layer] = a[n-layer-1][n-i-1];
        //         a[n-layer-1][n-i-1] = a[i][n-layer-1];
        //         a[i][n-layer-1] = tmp;
        //     }
        // }

        // for(int i=0;i<n;i++){
        //     for(int j=0;j<n;j++){
        //         System.out.print(a[i][j]);
        //     }
        //     System.out.println();
        // }

        // int n = nextInt(), m = nextInt();
        // int [][]a = new int[n][m];
        // for(int i=0;i<n;i++){
        //     String s = next();
        //     for(int j=0;j<m;j++){
        //         a[i][j] = s.charAt(j) - '0';    
        //     }
        // }

        // int row[] = new int[n];
        // int col[] = new int[m];
        // for(int i=0;i<n;i++){
        //     for(int j=0;j<m;j++){
        //         if (a[i][j] == 0) {
        //             row[i] = 1;
        //             col[j] = 1;
        //         }
        //     }
        // }

        // for(int i=0;i<n;i++){
        //     for(int j=0;j<m;j++){
        //         a[i][j] = 1;
        //         if (row[i] == 1 || col[j] == 1) {
        //             a[i][j] = 0;
        //         }
        //     }
        // }

        // for(int i=0;i<n;i++){
        //     for(int j=0;j<m;j++){
        //         System.out.print(a[i][j]);
        //     }
        //     System.out.println();
        // }

        out.close();	
    }

    private boolean isAnagram(String a, String b) {
        char []a1 = a.toCharArray();
        char []b1 = b.toCharArray();
        Arrays.sort(a1);
        Arrays.sort(b1);
        return new String(a1).equals(new String(b1));
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}