import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        List<Integer> first = dp(a, n);
        out.println(first);
        List<Integer> second = dp(a, 2 * n);
        out.println(second);

        out.close();
    }

    boolean [][]dp;
    int N;
    int []a;
    private List<Integer> dp(int []a, int N) {
        this.a = a;
        int n = a.length;
        this.N = N;
        dp = new boolean[n][N];
        dp[0][a[0] % N] = true;
        for(int i = 1; i < n; ++i) {
            dp[i][a[i] % N] = true;
            for(int j = 0; j < N; ++j) {
                dp[i][j] |= dp[i-1][j];
                dp[i][(j + a[i]) % N] |= dp[i-1][j];
            }
        }
        // for(int i = 0; i < n; ++i) {
        //     System.out.println(Arrays.toString(dp[i]));
        // }
        if (!dp[n-1][0]) return null;
        List<Integer> ret = new ArrayList<Integer>();
        backtrack(n - 1, 0, ret);

        return ret;
    }

    private void backtrack(int pos, int mod, List<Integer> r) {
        if (pos == 0) {
            if (mod == 0) return;
            r.add(a[pos]);
            return;
        }
        if (dp[pos][mod] == dp[pos - 1][mod]){
            backtrack(pos - 1, mod, r);
        }else {
            int nmod = mod - a[pos];
            if (nmod < 0) nmod += N;
            r.add(a[pos]);
            backtrack(pos - 1, nmod, r);
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
