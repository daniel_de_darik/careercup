import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	char []a = next().toCharArray();
	
	go(0, a.length-1, a);
        out.close();	
    }
    
    private void go(int l, int r, char []a){
	if (l >= r) return;
	int m = (l + r)/2;
	for(int i=l;i<=m;i++){
	    for(int j=m+1;j<=r;j++){
		out.println(a[i] + "" + a[j]);
	    }
	}
	go(l, m, a);
	go(m+1, r, a);
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}