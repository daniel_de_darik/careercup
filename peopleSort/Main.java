import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Treap root = null;
    Random rnd;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	int n = nextInt();
	rnd = new Random();
	
	Human h[] = new Human[n];
	
	for(int i=0;i<n;i++){
	    h[i] = new Human(nextInt(), nextInt(), next());
	}
	Arrays.sort(h);
	
	for(int i=0;i<n;i++){
	    add(h[i]);
	}
	
	print(root);
	System.out.println();
        out.close();
    }
    
    private void print(Treap tr){
	if (tr == null) return;
	print(tr.left);
	System.out.print(tr.name + " ");
	print(tr.right);
    }
    
    private void add(Human h){
	Treap t = new Treap(h.h, rnd.nextInt(2000000000), null, null, h.name);
	if (root == null){
	    root = t;
	    return;
	}
	
	int x = h.o;
	Treap m[] = split(root, x);
	root = merge(merge(m[0], t), m[1]);
    }
    
    public Treap merge(Treap l, Treap r){
	if (l == null) return r;
	if (r == null) return l;
	
	Treap t = null;
	if (l.y > r.y){
	    t = merge(l.right ,r);
	    t = new Treap(l.x, l.y, l.left, t, l.name);
	}else{
	    t = merge(l, r.left);
	    t = new Treap(r.x, r.y, t, r.right, r.name);
	}
	
	t.recalc();
	
	return t;
    }
    
    public Treap[] split(Treap tr, int x){
	int cur = sizeOf(tr.left) + 1;
	Treap []ret = new Treap[]{null, null};
	if (cur <= x){
	    if (tr.right != null){
		ret = split(tr.right, x - cur);
	    }
	    ret[0] = new Treap(tr.x, tr.y, tr.left, ret[0], tr.name);
	    ret[0].recalc();
	}else{
	    if (tr.left != null){
		ret = split(tr.left, x);
	    }
	    ret[1] = new Treap(tr.x, tr.y, ret[1], tr.right, tr.name);
	    ret[1].recalc();
	}
	
	return ret;
    }
    
    private int sizeOf(Treap tr){
	return (tr == null) ? 0 : tr.size;
    }
    
    static class Treap{
	public int x, y;
	public Treap left, right;
	public int size;
	public String name;
	
	public Treap(int x, int y, Treap left, Treap right, String name){
	    this.x = x;
	    this.y = y;
	    this.left = left;
	    this.right = right;
	    this.size = 1;
	    this.name = name;
	}
	
	public void recalc(){
	    size = sizeOf(left) + sizeOf(right) + 1;
	}
	
	public int sizeOf(Treap tr){
	    return (tr == null) ? 0 : tr.size;
	}
    }
    
    static class Human implements Comparable<Human>{
	public int h, o;
	public String name;
    
	public Human(int h, int o, String name){
	    this.h = h;
	    this.o = o;
	    this.name = name;
	}
	
	public int compareTo(Human other){
	    if (this.h > other.h) return -1;
	    if (this.h < other.h) return 1;
	    return 0;
	}
    }
    
    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}