/*
Given list of words @input1
Given relative importance of words @input2
Remove all less important words in @input1
Ex1:
@input1 A, B, C
@input2 A > B
@output A, C

Ex2:
@niput1 A, B, C
@input2 A > B, B > C
@output A
*/
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        String []a = new String[n];
        for(int i = 0; i < n; ++i) a[i] = next();
        
        int m = nextInt();
        Map<String, String> map = new HashMap<String, String>();
        for(int i = 0; i < m; ++i) {
            String u = next(), v = next();
            map.put(u, v);
        }

        List<String> r = process(a, map);
        out.println(r);
        out.close();
    }

    int n;
    List<Integer> []g;
    int []tin, tout, color;
    int tick = 0;

    private List<String> process(String []a, Map<String, String> dict) {
        List<String> l = new ArrayList<>();
        for(String i : dict.keySet()) {
            l.add(i);
            l.add(dict.get(i));
        }
        Collections.sort(l);
        int at = 0;
        for(int i = 1; i < l.size(); ++i) {
            if (l.get(at).compareTo(l.get(i)) != 0) {
                l.set(++at, l.get(i));
            }
        }

        for(int i = l.size() - 1; i > at; --i) {
            l.remove(i);
        }
        // System.out.println(l);
        n = l.size();
        g = new LinkedList[n];
        for(int i = 0; i < n; ++i) g[i] = new LinkedList<>();
        for(String i : dict.keySet()) {
            int v = Collections.binarySearch(l, i), u = Collections.binarySearch(l, dict.get(i));
            g[v].add(u);
        }

        color = new int[n];
        tin = new int[n];
        tout = new int[n];
        for(int i = 0; i < n; ++i) {
            if (color[i] == 0) {
                if (dfs(i)) {
                    out.println("Wrong importance provided");
                    return new ArrayList<>();
                }
            }
        }

        // System.out.println(Arrays.toString(tin));
        // System.out.println(Arrays.toString(tout));

        int m = a.length;
        boolean []used = new boolean[m];
        boolean updated = true;
        while(updated) {
            updated = false;
            for(int i = 0; i < m; ++i) if (!used[i]) {
                int v = Collections.binarySearch(l, a[i]);
                if (v < 0) continue;
                for(int j = 0; j < m; ++j) if (!used[j] && i != j) {
                    int u = Collections.binarySearch(l, a[j]);
                    if (u < 0) continue;
                    if (moreImportant(v, u)) {
                        updated = true;
                        used[j] = true;
                    }
                }
            }
        }

        Set<String> r = new HashSet<String>();
        for(int i = 0; i < m; ++i) {
            if (!used[i]) {
                r.add(a[i]);
            }
        }

        return new ArrayList<String>(r);
    }

    private boolean moreImportant(int v, int u) {
        return tin[v] < tin[u] && tout[u] < tout[v];
    }

    private boolean dfs(int x) {
        color[x] = 1;
        tin[x] = tick++;
        for(int i : g[x]) {
            if (color[i] == 0) {
                if (dfs(i)) {
                    return true;
                }
            }else if (color[i] == 1) {
                return true;
            }
        }
        color[x] = 2;
        tout[x] = tick++;
        return false;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
