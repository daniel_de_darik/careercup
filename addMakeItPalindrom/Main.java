import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    char []s;
    int suff, n;
    int size = 0;
    Node []tree;

    final static int MAX = 100000;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        StringBuilder sb = new StringBuilder();
        sb.append(next());
        sb.reverse();
        s = sb.toString().toCharArray();
        System.out.println(Arrays.toString(s));
        n = s.length;
        init();

        for(int i = 0; i < n; ++i) addLetter(i);
        int len = tree[suff].len;
        out.println(n - len);
        out.close();
    }

    private boolean addLetter(int pos) {
        int cur = suff, curlen = -1;
        int to = s[pos] - 'a';

        while(true) {
            curlen = tree[cur].len;
            if (pos - curlen - 1 >= 0 && s[pos - curlen - 1] == s[pos]) {
                break;
            }
            cur = tree[cur].sufflink;
        }
        if (tree[cur].next[to] != -1) {
            suff = tree[cur].next[to];
            return false;
        }

        int v = size++;
        suff = v;
        tree[v] = new Node(tree[cur].len + 2);
        tree[cur].next[to] = v;

        if (tree[v].len == 1) {
            tree[v].sufflink = 1;
            return true;
        }

        while(true) {
            cur = tree[cur].sufflink;
            curlen = tree[cur].len;
            if (pos - curlen - 1 >= 0 && s[pos - curlen - 1] == s[pos]) {
                tree[v].sufflink = tree[cur].next[to];
                break;
            }
        }

        return true;
    }

    private void init() {
        tree = new Node[MAX];
        tree[size] = new Node(-1);
        tree[size].sufflink = size++;

        tree[size] = new Node(0);
        tree[size++].sufflink = 0;

        suff = size - 1;
    }

    class Node {
        int next[];
        int sufflink;
        int len;

        public Node(int len) {
            this.len = len;
            sufflink = -1;
            this.len = len;
            next = new int[26];
            Arrays.fill(next, -1);
        }

        public String toString() {
            return "[len=" + this.len + ", sufflink=" + this.sufflink + "]";
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
