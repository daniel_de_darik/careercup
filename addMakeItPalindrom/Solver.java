import java.util.*;
import java.io.*;
	
public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    final static long PRIME = 31;
    
	long []p, h, hr;
    int n;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        char []a = next().toCharArray();
        n = a.length;
        if (n == 1) {
            out.println(0);
            out.close();
            return;
        }
        p = new long[n];
        p[0] = 1;
        for(int i = 1; i < n; ++i) p[i] = p[i-1] * PRIME;

        h = new long[n];
        hr = new long[n];
        for(int i = 0; i < n; ++i) {
            h[i] = p[i] * (a[i] - 'a' + 1);
            hr[i] = p[i] * (a[n - i - 1] - 'a' + 1);
            if (i > 0) {
                h[i] += h[i-1];
                hr[i] += hr[i-1];
            }
        }

        int ret = n - 1;
        for(int i = n / 2 - 1; i + 1 < n; ++i) {
            int len = Math.min(i + 1, n - i - 1);
            long left = h[i];
            if (i - len >= 0) left -= h[i - len];
            long right = hr[len - 1];
            if (left * p[n - i - 1] == right * p[n - (len - 1) - 1]) {
                ret = Math.min(ret, n - 2 * len);
            }

            len = Math.min(i + 1, n - i - 2);
            if (len == 0) continue;
            left = h[i];
            if (i - len >= 0) left -= h[i - len];
            right = hr[len - 1];
            if (left * p[n - i - 1] == right * p[n - (len - 1) - 1]) {
                ret = Math.min(ret, n - (2 * len + 1));
            }
        }
        
        out.println(ret);
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Solver().run();
    }
}
