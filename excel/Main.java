import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        String s = num2alph(n);

        out.println(n + " -> " + s);
        out.println(s + " -> " + alph2num(s));

        out.close();	
    }

    private String num2alph(int n) {
        String s = "";
        s+=(char)('')

        return s;
    }

    private int alph2num(String s) {
        int r = 0;  
        int p = 1;
        int base = 26;
        for(int i=0;i<s.length();i++){
            int x = s.charAt(s.length()-i-1) - 'a' + 1;
            if (i == 0) x--;
            r+=x * p;
            p*=base;
        }

        return r;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}