import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	// int xx = 2147483648;
    	// System.out.println(xx);

        int x = -5;
        int y = 6;
        int res = 0;
        int parity = 0;
        for(int i=0;i<33;i++){
            int c = 0;
            if ((x & (1 << i)) > 0) {
                c = 1;
            }
            if ((y & (1<<i)) > 0) {
                if (c == 1) c = 2;
                else c = 1;
            }
            
            if (c == 2) {
                if (parity == 1) {
                    res|=1<<i;    
                }
                parity = 1;
                
            }else if (c == 1) {
                if (parity == 1) {
                    parity = 1;
                }else {
                    parity = 0;
                    res|=1<<i;
                }
            }else {
                if (parity == 1) {
                    res|=1<<i;
                }
                parity = 0;
            }
        }

        System.out.println(res);
        System.out.println(add(x, y));
        int n = 1;
        for(int i=0;i<32;i++){
            if ((n & (1<<i)) > 0) {
                out.print(1);
            }else{
                out.print(0);
            }
        }
        out.println();
        out.println((n >> 31) & 1);
        out.close();	
    }

    private int add(int a, int b) {
        if (b == 0) return a;
        int sum = a ^ b;
        int carry = (a & b) << 1;

        return add(sum, carry);
    }

    private int count(int a) {
        if (a == 0) return 0;
        int c = a & 1;
        return c + count(a >> 1);
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}