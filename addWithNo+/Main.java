import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int a = nextInt(), b = nextInt();
	int c = add(a, b);
	out.println(c);
        out.close();
    }
    
    private int add(int a, int b){
	if (b == 0) return a;
	if (a == 0) return b;
	int r = a ^ b;
	int c = (a & b) << 1;
	return add(r, c);
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}