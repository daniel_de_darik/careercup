import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }

    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        System.out.println(countPrimes(n));
        out.close();
    }

    final static int MAXN = 5000010;
    final static int BLOCK = 10000;
    final static double EPS = 1e-9;

    int []prime;
    boolean []nprime, bl;

    public int countPrimes(int N) {
        if (N <= 1) return 0;

        int sqrt = (int)Math.floor(Math.sqrt(MAXN) + EPS);
        nprime = new boolean[sqrt + 1];
        int size = 0;
        prime = new int[10000];
        for(int i = 2; i <= sqrt; ++i) {
            if (!nprime[i]) {
                prime[size++] = i;
                if (1L * i * i <= sqrt) {
                    for(int j = i * i; j <= sqrt; j +=i) {
                        nprime[j] = true;
                    }
                }
            }
        }
        int ret = 0;
        bl = new boolean[BLOCK];
        for(int k = 0; k <= MAXN / BLOCK; ++k) {
            Arrays.fill(bl, false);
            int start = k * BLOCK;
            for(int i = 0; i < size; ++i) {
                int idx = (start + prime[i] - 1) / prime[i];
                int j = Math.max(idx, 2) * prime[i] - start;
                while(j < BLOCK) {
                    bl[j] = true;
                    j += prime[i];
                }
            }

            if (k == 0) bl[0] = bl[1] = true;
            for(int j = 0; j < BLOCK && start + j < N; ++j) {
                if (!bl[j]) ++ret;
            }
        }

        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Solver().run();
    }
}