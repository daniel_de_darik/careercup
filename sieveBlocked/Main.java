import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }

    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        System.out.println(countPrimes(n));
        out.close();
    }

    final static int MAXN = 5000010;
    final static double EPS = 1e-9;
    final static int BLOCK = 10000;

    int []primes;
    boolean []nprime, bl;

    public int countPrimes(int N) {
        if (N <= 1) return 0;
        int sqrt = (int)Math.floor(Math.sqrt(N) + EPS);
        primes = new int[10000];
        nprime = new boolean[sqrt + 1];

        int at = 0;
        for(int i = 2; i <= sqrt; ++i) {
            if (!nprime[i]) {
                primes[at++] = i;
                if (1L * i * i <= sqrt) {
                    for(int j = i * i; j <= sqrt; j += i) {
                        nprime[j] = true;
                    }
                }
            }
        }

        int ret = 0;
        bl = new boolean[BLOCK];
        for(int k = 0; k <= MAXN / BLOCK; ++k) {
            Arrays.fill(bl, false);
            int start = k * BLOCK;
            // System.out.println("start=" + start);
            for(int i = 0; i < at; ++i) {
                int idx = (start + primes[i] - 1) / primes[i];
                int j = primes[i] * Math.max(idx, 2) - start;
                for(;j < BLOCK; j += primes[i]) {
                    bl[j] = true;
                }
            }
            if (k == 0) bl[0] = bl[1] = true;
            // System.out.println(Arrays.toString(bl));
            for(int i = 0; i < BLOCK && start + i < N; ++i) {
                if (!bl[i]) ++ret;
            }
        }

        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}