import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();

        Node root = new Node(a[n-1]);
        System.out.println(0);
        for(int i=n-2;i>=0;i--){
            add(a[i], root);
        }

        out.close();	
    }

    private void add(int x, Node root) {
        Node elm = new Node(x);
        Node cur = root, parent = null;
        boolean isLeft = false;
        int count = 0;
        while(true) {
            parent = cur;
            if (cur.key < x) {
                count+=cur.children + 1;
                isLeft = false;
                cur = cur.right;
            }else {
                cur.children++;
                isLeft = true;
                cur = cur.left;
            }
            if (cur == null) {
                if (isLeft) parent.left = elm;
                else parent.right = elm;
                System.out.println(count);
                return;
            }
        }
    }

    static class Node {
        int children, key;
        Node left, right;

        public Node(int key) {
            this.key = key;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}