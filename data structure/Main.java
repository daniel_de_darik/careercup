import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
		MyDs ds = new MyDs();
		int []a = new int[]{1, 5, 3, 10, 4, 7, 4, 4, 4};
		for(int i=0;i<a.length;i++) ds.add(a[i]);
		ds.remove(4);
		ds.remove(4);
		ds.remove(4);
		ds.remove(4);
		int c = 0;
		while(c++ < 10){
		    int x = ds.random();
		    System.out.println(x);
		}
	
        out.close();	
    }
    
    static class MyDs{
		Map<Integer, Set<Integer>> map;
		int []a;
		int size;
		Random rnd;
		
		public MyDs(){
		    map = new HashMap<Integer, Set<Integer>>();
		    a = new int[1000];
		    size = 0;
		    rnd = new Random();
		}
		
		public void add(int x){
		    a[size] = x;
		    if (!map.containsKey(x)){
				map.put(x, new HashSet<Integer>());
		    }
		    Set<Integer> set = map.get(x);
		    set.add(size);
		    //map.put(x, set);
		    size++;
		}
		
		public void remove(int x){
		    if (!map.containsKey(x)) return;
		    Set<Integer> set = map.get(x);
		    if (set.size() == 0) return;
		    Iterator<Integer> it = set.iterator();
		    int idx = it.next();
		    set.remove(idx);
		    if (set.size() == 0) map.remove(x);
		    
		    a[idx] = a[size-1];
		    set = map.get(a[idx]);
		    set.remove(size-1);
		    set.add(idx);
		    //map.put(a[idx], set);
		    size--;
		}
		
		public boolean contains(int x){
		    return map.containsKey(x);
		}
		
		public int random(){
		    int x = rnd.nextInt(size);
		    return a[x];
		}
    }
    
    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}