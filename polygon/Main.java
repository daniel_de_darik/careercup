import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    int n;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

	n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	char []b = new char[n];
	for(int i=0;i<n;i++) b[i] = next().charAt(0);
	
	V []v = new V[n];
	for(int i=0;i<n;i++){
	    v[i] = new V(a[i], b[i], b[(i+1)%n]);
	}
	
	int max = Integer.MIN_VALUE;
	
	for(int i=0;i<n;i++){
	    V[] x = new V[n-1];
	    int yk = 0;
	    for(int j=0;j<n;j++){
		if (i == j) continue;
		x[yk++] = v[j];
	    }
	    
	    int ret = maximize(0, yk, x);
	    if (v[i].left == '+' || v[i].right == '+') max = Math.max(max, process('+', ret, v[i].key));
	    if (v[i].left == '*' || v[i].right == '*') max = Math.max(max, process('*', ret, v[i].key));
	}

        out.close();
    }
    
    private int maximize(int l, int r, V[] v){
	if(l == r) return v[l].key;
	if (r - l == 1) return process(v[l].right, v[l].key, v[r].key);
	int max = Integer.MIN_VALUE;
	for(int i=l;i<r;i++){
	    int left = maximize(l, i, v);
	    int right = maximize(i+1, r, v);
	    if (v[i].right == '+'){
		max = Math.max(max, left + right);
	    }else{
		max = Math.max(max, left * right);
		
		left = minimize(l, i, v);
		right = minimize(i+1, r, v);
		
		max = Math.max(max, left * right);
	    }
	}
	
	return max;
    }
    
    private int minimize(int l, int r, V[] v){
	if (l == r) return v[l].key;
	if (r - l == 1) return process(v[l].right, v[l].key, v[r].key);
	int min = Integer.MAX_VALUE;
	for(int i=l;i<r;i++){
	    int left = minimize(l, i, v);
	    int right = minimize(i+1, r, v);
	    if (v[i].right == '+'){
		min = Math.min(min, left + right);
	    }else{
		int leftmax = maximize(l, i, v);
		int rightmax = maximize(i+1, r, v);
		
		min = Math.min(min, left * rightmax);
		min = Math.min(min, right * leftmax);
	    }
	}
	
	return min;
    }
    
    private int process(char op, int a, int b){
	if (op == '+') return a + b;
	return a * b;
    }
    
    static class V{
	int key;
	char left, right;
	
	public V(int key, char left, char right){
	    this.key = key;
	    this.left = left;
	    this.right = right;
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}