import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        List<String[]> r = solveNQueens(n);
        for(String[] i : r) {
            System.out.println(Arrays.toString(i));
        }
        out.close();
    }

    public List<String[]> solveNQueens(int n) {
        int []a = new int[n];
        char [][]maze = new char[n][n];
        for(int i = 1; i < n; ++i) a[i] = i;
        List<String[]> ret = new LinkedList<String[]>();
        while(a.length > 0){
            boolean valid = true;
            main:for(int i = 0; i < a.length; ++i){
                for(int j = i + 1; j < a.length; ++j){
                    if (Math.abs(i - j) == Math.abs(a[i] - a[j])) {
                        valid = false;
                        break main;
                    }
                }
            }
            if (valid){
                for(char []i : maze) Arrays.fill(i, '.');
                for(int i = 0; i < n; ++i) maze[i][a[i]] = 'Q';
                String []r = new String[n];
                for(int i = 0; i < n; ++i) r[i] = new String(maze[i]);
                ret.add(r);
            }
            a = nextPermutation(a);
        }
        return ret;
    }
    
    private int[] nextPermutation(int a[]){
        int n = a.length, k = n - 2;
        if (n == 1) return new int[]{};
        while(k >= 0 && a[k] > a[k+1]){
            --k;
            if (k < 0) return new int[]{};
        }
        int idx = -1;
        for(int i = k + 1; i < n; ++i){
            if (a[i] > a[k] && (idx == -1 || a[idx] > a[i])){
                idx = i;
            }
        }
        int tmp = a[k];
        a[k] = a[idx];
        a[idx] = tmp;
        
        for(int i = k + 1, j = n - 1; i < j; ++i, --j){
            tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
        
        return a;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
