import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int s = 0;
        for(int i=0;i<=n;i++){
            s+=count(i);
        }

        int s2 = count2s(n);

        System.out.println(s + " " + s2);

        out.close();	
    }

    private int count2s(int n) {
        if (n < 10) {
            if (n < 2) return 0;
            return 1;
        }

        int pow = 1;
        while(10 * pow < n) pow*=10;
        int f = n/pow;
        int rem = n % pow;

        int ret = 0;

        if (f > 2) ret+=pow;
        else if (f == 2) ret+=rem + 1;

        ret+=f * count2s(pow-1) + count2s(rem);

        return ret;
    }

    private int count(int x) {
        int ret = 0;
        while(x > 0){
            int rem = x % 10;
            x/=10;
            if (rem == 2) ret++;
        }

        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}