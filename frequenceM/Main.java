import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int m = 3;
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();
        // int [][]res = new int[2][2];
        // for(int i=0;i<n;i++){
        //     if (res[0][0] == 0) {
        //         res[0][0] = a[i];
        //         res[0][1] = 1;
        //     }else if (res[1][0] == 0) {
        //         res[1][0] = a[i];
        //         res[1][1] = 1;
        //     }else if (res[0][0] == a[i]) {
        //         res[0][1]++;
        //     }else if (res[1][0] == a[i]) {
        //         res[1][1]++;
        //     }else {
        //         res[0][1]--;
        //         res[1][1]--;

        //         if (res[0][1] == 0) {
        //             res[0][0] = 0;
        //         }
        //         if (res[1][1] == 0) {
        //             res[1][0] = 0;
        //         }
        //     }
        // }
        
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for(int i=0;i<n;i++){
            if (!map.containsKey(a[i])) {
                map.put(a[i], 0);
            }
            map.put(a[i], map.get(a[i]) + 1);
            if (map.size() == m) {
                map.remove(a[i]);
                List<Integer> list = new ArrayList<Integer>();
                for(int elm : map.keySet()) list.add(elm);
                for(int elm : list){
                    int x = map.get(elm);
                    if (x == 1) map.remove(elm);
                    else map.put(elm, x - 1);
                }
            }
        }

        for(int elm : map.keySet()){
            int c = 0;
            for(int i=0;i<n;i++){
                if (a[i] == elm) c++;
            }
            if (c > n/3) {
                out.print(elm + " ");
            }
        }
        
        out.println();
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}