import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Random rnd;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++){
	    a[i] = nextInt();
	}
	
	rnd = new Random();
	
	int []b = (int[])a.clone();
	a = mergeSort(0, n-1, a);
	out.println(Arrays.toString(a));
	Arrays.sort(b);
	out.println(Arrays.toString(b));
        out.close();	
    }
    
    private int[] mergeSort(int l, int r, int []a){
		if (l > r) return new int[]{};
		if (l == r) return new int[]{a[l]};
		
		int m = l + rnd.nextInt(r - l + 1);
		int left[] = mergeSort(l, m, a);
		int right[] = mergeSort(m + 1, r, a);
		int res[] = new int[left.length + right.length];
		
		int i = 0, j = 0, yk = 0;
		while(i < left.length && j < right.length){
		    if (left[i] < right[j]){
				res[yk++] = left[i++];
		    }else if (left[i] > right[j]){
				res[yk++] = right[j++];
		    }else{
				res[yk++] = right[j++];
				res[yk++] = left[i++];
		    }
		}
		
		for(;i<left.length;i++) res[yk++] = left[i];
		for(;j<right.length;j++) res[yk++] = right[j];
		
		return res;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}