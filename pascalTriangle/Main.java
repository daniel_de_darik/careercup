import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        List<List<Integer>> ret = generate(n);
        for(List<Integer> i : ret) {
            System.out.println(i);
        }
        out.close();
    }

    public List<List<Integer>> generate(int n) {
        List<List<Integer>> ret = new LinkedList<>();
        if (n == 0) return ret;
        int [][]c = new int[n][n];
        c[0][0] = 1;
        List<Integer> lst = new LinkedList<>();
        lst.add(1);
        ret.add(lst);
        for(int i = 1; i < n; ++i){
            c[i][0] = c[i][i] = 1;
            for(int j = 1; j < i; ++j)
                c[i][j] = c[i-1][j-1] + c[i-1][j];
            lst = new LinkedList<>();
            for(int j = 0; j <= i; ++j) lst.add(c[i][j]);
            ret.add(lst);
        }
        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
