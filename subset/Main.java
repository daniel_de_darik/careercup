import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    
    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));

    	char []a = next().toCharArray();
        int yk = 0;
        int index[] = new int[a.length];
        for(int i=0;i<a.length;i++) {
            char c = a[i];
            if (c >= '0' && c <='9') continue;
            index[yk++] = i;
        }

        for(int mask = 0; mask < (1<<yk);mask++){
            char []c = (char[])a.clone();
            for(int i=0;i<yk;i++){
                if ((mask & (1<<i)) > 0) {
                    c[index[i]] = Character.toUpperCase(c[index[i]]);
                }
            }
            System.out.println(new String(c));
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}