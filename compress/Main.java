import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	char []a = next().toCharArray();
	int yk = 0;
	int cnt = 1;
	for(int i=1;i<a.length;i++){
	    if (a[i-1] == a[i]){
		cnt++;
	    }else{
		a[yk++] = a[i-1];
		if (cnt > 1){
		    int len = 0;
		    int x = cnt;
		    while(x > 0){
			x/=10;
			len++;
		    }
		
		    int j = yk + len - 1;
		    x = cnt;
		    while(x > 0){
			a[j--] = (char)(x % 10 + '0');
			x/=10;
		    }
		    yk+=len;
		}
		cnt = 1;
	    }
	}
	a[yk++] = a[a.length-1];
	if (cnt > 1){
	    int len = 0;
	    int x = cnt;
	    while(x > 0){
		x/=10;
		len++;
	    }
	    int j = yk + len - 1;
	    x = cnt;
	    while(x > 0){
		a[j--] = (char)(x % 10 + '0');
		x/=10;
	    }
	    yk+=len;
	}
	for(int i=0;i<yk;i++){
	    out.print(a[i]);
	}
	out.println();
        out.close();	
    }
    

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}