import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt(), m = nextInt(), k = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        int []b = new int[m];
        for(int i = 0; i < m; ++i) b[i] = nextInt();

        int l = -1, r = Math.min(k, n);
        while(r - l > 1) {
            
        }

        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
