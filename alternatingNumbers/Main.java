import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n + 1];
        for(int i = 1; i <= n; ++i) a[i] = nextInt();
        for(int i = 2; i <= n; ++i) siftUp(i, a);

        dfs(1, a);
        System.out.println();

        out.close();
    }

    private void dfs(int i, int []a) {
        if (i > a.length) return;
        if (2 * i < a.length) {
            dfs(2 * i, a);
        }
        System.out.print(a[i] + " ");
        if (2 * i + 1 < a.length) {
            dfs(2 * i + 1, a);
        }
    }

    private void siftUp(int i, int []a) {
        int n = a.length;
        while(i > 1 && a[i / 2] > a[i]) {
            int tmp = a[i];
            a[i] = a[i/2];
            a[i/2] = tmp;
            i /= 2;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
