import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	String s = next();
    	List<List<String>> r = partition(s);
    	for(List<String> l : r) System.out.println(l);

        out.close();
    }

    int n;
    char []a;
    boolean [][]c, dp;
    List<List<String>>ret;
    
    public List<List<String>> partition(String s) {
        n = s.length();
        a = s.toCharArray();
        c = new boolean[n][n];
        for(int i = 0; i < n; ++i) c[i][i] = true;
        for(int i = 0; i + 1 < n; ++i) if (a[i] == a[i+1]) c[i][i+1] = true;
        for(int  l = 3; l <= n; ++l){
            for(int i = 0; i <= n - l; ++i){
                if (a[i] == a[i + l - 1]) c[i][i + l - 1] = c[i+1][i + l - 2];
            }
        }

        dp = new boolean[n][n];
        for(int i = 0; i < n; ++i) if (c[0][i]) dp[0][i] = true;
        
        for(int k = 1; k < n; ++k){
            for(int i = 0; i < n; ++i){
                for(int j = 0; j < i; ++j){
                    if (dp[k-1][j] && c[j + 1][i]){
                        dp[k][i] = true;
                    }
                }
            }
        }
        
        ret = new LinkedList<>();
        for(int i = 0; i < n; ++i){
            if (dp[i][n-1]){
                backtrack(i, n - 1, new LinkedList<String>());
            }
        }
        
        return ret;
    }
    
    private String toStr(int l, int r){
        StringBuilder sb = new StringBuilder();
        for(int i = l; i <= r; ++i) sb.append(a[i]);
        return sb.toString();
    }
    
    private void backtrack(int k, int pos, LinkedList<String> lst){
        if (k == 0){
            List<String> cloned = (List<String>)lst.clone();
            cloned.add(toStr(0, pos));
            Collections.reverse(cloned);
            ret.add(cloned);
            return;
        }
        for(int i = pos - 1; i >= 0; --i){
            if (dp[k - 1][i] && c[i + 1][pos]){
                LinkedList<String> cloned = (LinkedList<String>)lst.clone();
                cloned.add(toStr(i + 1, pos));
                backtrack(k - 1, i, cloned);
            }
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
