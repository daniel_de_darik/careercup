import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	char[]a = next().toCharArray();
	int n = a.length;
	int i = 0, yk = 0;
	while(i < n){
	    if (a[i] == 'c'){
		if (yk > 0 && a[yk-1] == 'a'){
		    yk--;
		}else{
		    a[yk++] = a[i];
		}
	    }else{
		if (a[i] != 'b'){
		    a[yk++] = a[i];
		}
	    }
	    i++;
	}
	for(i=0;i<yk;i++){
	    out.print(a[i]);
	}
	out.println();
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}