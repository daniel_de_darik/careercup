import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        Point p[] = new Point[n];
        for(int i = 0; i < n; ++i) p[i] = new Point(nextInt(), nextInt());
        System.out.println(maxPoints(p));
        out.close();
    }

    public int maxPoints(Point[] p) {    
        int n = p.length;
        if (n <= 2) return n;
        Set<Triple> set = new HashSet<Triple>();
        for(int i = 0; i < n; ++i){
            for(int j = i + 1; j < n; ++j){
                int a = p[j].y - p[i].y, b = p[i].x - p[j].x, c = -a * p[i].x - b * p[i].y;
                if (a == 0 && b == 0) continue;
                int gcd = gcd(Math.abs(a), Math.abs(b));
                gcd = gcd(gcd, Math.abs(c));
                a/=gcd;
                b/=gcd;
                c/=gcd;
                if (a < 0){
                    a = -a;
                    b = -b;
                    c = -c;
                }else if (a == 0){
                    if (b < 0){
                        b = -b;
                        c = -c;
                    }
                }
                Triple t = new Triple(a, b, c);
                set.add(t);
                System.out.println(p[i] + ", " + p[j] + ", " + t);
            }
        }
        int max = 2;
        for(Triple t : set){
            System.out.println(t);
            long a = t.a, b = t.b, c = t.c;
            int count = 0;
            for(int i = 0; i < n; ++i){
                if (a * p[i].x + b * p[i].y + c == 0) ++count;
            }
            max = Math.max(max, count);
        }
        return max;
    }

    private int gcd(int a, int b){
        return (b == 0) ? a : gcd(b, a % b);
    }
    
    class Triple{
        int a, b, c;
        public Triple(int a, int b, int c){
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public int hashCode(){
            int []d = new int[]{a, b, c};
            return Arrays.hashCode(d);
        }
        
        public boolean equals(Object obj){
            Triple other = (Triple)obj;
            return this.a == other.a && this.b == other.b && this.c == other.c;
        }

        public String toString() {
            return this.a + " " + this.b + " " + this.c;
        }
    }

    class Point {
        int x;
        int y;
        Point() { x = 0; y = 0; }
        Point(int a, int b) { x = a; y = b; }

        public String toString(){ 
            return this.x + " " + this.y;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
