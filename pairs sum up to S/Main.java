import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt(), s = nextInt();
	
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	a = mergeSort(0, n-1, a);
	//System.out.println(Arrays.toString(a));
	
	List<P> lst = new ArrayList<P>();
	
	int l = 0, r = n - 1;
	while(l < r){
	    if (a[l] + a[r] > s){
		r--;
	    }else if (a[l] + a[r] < s){
		l++;
	    }else{
		List<Integer> left = new ArrayList<Integer>();
		List<Integer> right = new ArrayList<Integer>();
		int ll = l, rr = r;
		while(ll < r && a[l] == a[ll]){
		    left.add(ll);
		    ll++;
		}
		while(rr > l && a[r] == a[rr]){
		    right.add(rr);
		    rr--;
		}
		for(int i=0;i<left.size();i++){
		    for(int j=0;j<right.size();j++){
			if (left.get(i) < right.get(j)){
			    lst.add(new P(left.get(i), right.get(j)));
			}
		    }
		}
		l = ll;
		r = rr;
	    }
	}
	Collections.sort(lst);
	for(P i : lst)
	    out.println(i);
        out.close();
    }
    
    private int[] mergeSort(int l, int r, int []a){
	if (l > r) return new int[]{};
	if (l == r) return new int[]{a[l]};
	int m = (l + r) >> 1;
	int left[] = mergeSort(l, m, a);
	int right[] = mergeSort(m+1, r, a);
	int atl = 0, atr = 0, at = 0;
	int []ret = new int[r - l + 1];
	while(atl < left.length && atr < right.length){
	    if (left[atl] < right[atr]){
		ret[at++] = left[atl++];
	    }else if (left[atl] > right[atr]){
		ret[at++] = right[atr++];
	    }else{
		ret[at++] = left[atl++];
		ret[at++] = right[atr++];
	    }
	}
	while(atl < left.length) ret[at++] = left[atl++];
	while(atr < right.length) ret[at++] = right[atr++];
	
	return ret;
    }
    
    class P implements Comparable<P>{
	int a, b;
	public P(int a, int b){
	    this.a = a;
	    this.b = b;
	}
	
	public int compareTo(P other){
	    if (this.a < other.a) return -1;
	    if (this.a > other.a) return 1;
	    if (this.b < other.b) return -1;
	    if (this.b > other.b) return 1;
	    return 0;
	}
	
	@Override
	public String toString(){
	    return this.a + " " + this.b;
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}