#!/bin/bash
javac Main.java
javac Solver.java
javac gen.java

COUNT=0

while(( "$COUNT" <= 100 ));do
    java gen >in.txt
    java Main <in.txt >out1.txt
    java Solver <in.txt >out2.txt
    
    diff out1.txt out2.txt
    if (( "$?" == 1 ));then
	echo "WRONG ANSWER $COUNT"
	break
    else
	echo "ACCEPTED $COUNT"
    fi
    COUNT=$[$COUNT+1]
done