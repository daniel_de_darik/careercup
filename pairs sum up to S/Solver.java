import java.util.*;
public class Solver{

    public void run(){
	Scanner in = new Scanner(System.in);
	int n = in.nextInt(), s = in.nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = in.nextInt();
	Arrays.sort(a);
	for(int i=0;i<n;i++){
	    for(int j=i+1;j<n;j++){
		if (a[i] + a[j] == s){
		    System.out.println(i + " " + j);
		}
	    }
	}
    }

    public static void main(String []args){
	new Solver().run();
    }
}