import java.util.*;
public class gen{

    public void run(){
	Random rnd = new Random();
	int n = rnd.nextInt(20) + 3;
	System.out.println(n);
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = rnd.nextInt(20);
	int s = -1;
	Map<Integer, Integer> map = new HashMap<Integer, Integer>();
	for(int i=0;i<n;i++){
	    for(int j=i+1;j<n;j++){
		int sum = a[i] + a[j];
		if (!map.containsKey(sum)){
		    map.put(sum, 0);
		}
		map.put(sum, map.get(sum) + 1);
	    }
	}
	int max = 0;
	for(int key : map.keySet()){
	    if (max < map.get(key)){
		max = map.get(key);
		s = key;
	    }
	}
	System.out.println(s);
	for(int i=0;i<n;i++) System.out.print(a[i] + " ");
	System.out.println();
    }

    public static void main(String args[]){
	new gen().run();
    }
}