/*
'.' Matches any single character.
'*' Matches zero or more of the preceding element.

The matching should cover the entire input string (not partial).

The function prototype should be:
bool isMatch(const char *s, const char *p)

Some examples:
isMatch("aa","a") → false
isMatch("aa","aa") → true
isMatch("aaa","aa") → false
isMatch("aa", "a*") → true
isMatch("aa", ".*") → true
isMatch("ab", ".*") → true
isMatch("aab", "c*a*b") → true
*/
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        String s = next(), p = next();
        System.out.println(isMatch(s, p) + " " + isMatch1(s, p));

        out.close();
    }

    public boolean isMatch(String s, String p){
        int n = s.length(), m = p.length();
        boolean [][]dp = new boolean[n + 1][m + 1];
        dp[0][0] = true;
        for(int j = 1; j <= m; ++j) {
            if (p.charAt(j - 1) == '*') {
                int k = j;
                while(k > 0 && p.charAt(k - 1) == '*') --k;
                dp[0][j] = dp[0][k-1];
            }
        }
        for(int i = 1; i <= n; ++i) {
            for(int j = 1; j <= m; ++j) {
                if (s.charAt(i - 1) == p.charAt(j - 1) || p.charAt(j - 1) == '.') {
                    dp[i][j] = dp[i-1][j-1];
                }else {
                    if (p.charAt(j - 1) == '*') {
                        int k = j;
                        while(k > 0 && p.charAt(k - 1) == '*') --k;
                        dp[i][j] |= dp[i][k-1];
                        if (p.charAt(k - 1) == s.charAt(i - 1) || p.charAt(k - 1) == '.') {
                            dp[i][j] |= dp[i-1][k] || dp[i-1][k-1] || dp[i-1][j];
                        }
                    }
                }
            }
        }

        for(boolean []i : dp)
            System.out.println(Arrays.toString(i));

        return dp[n][m];
    }

    public boolean isMatch1(String s, String p) {
        n = s.length();
        m = p.length();
        this.s = s;
        this.p = p;
        dp = new int[n][m];
        for(int []i : dp) Arrays.fill(i, -1);
        return dp(0, 0);
    }

    int [][]dp;
    int n, m;
    String s, p;

    private boolean dp(int i, int j){
        if (i == n){
            if (j == m) return true;
            return check(j);
        }
        if (j == m) return false;
        if (dp[i][j] != -1) return dp[i][j] == 1;
        if (j + 1 < m && p.charAt(j + 1) == '*') {
            if (dp(i, j + 2)) {
                dp[i][j] = 1;
                return true;
            }
        }
        if (s.charAt(i) == p.charAt(j) || p.charAt(j) == '.'){
            if (dp(i + 1, j + 1)){
                dp[i][j] = 1;
                return true;
            }
        }else{
            if (p.charAt(j) == '*'){
                if (dp(i, j + 1)){
                    dp[i][j] = 1;
                    return true;
                }
                int k = j;
                while(k >= 0 && p.charAt(k) == '*') --k;
                if (k >= 0 && s.charAt(i) == p.charAt(k) || p.charAt(k) == '.'){
                    if (dp(i + 1, j) || dp(i + 1, j + 1)){
                        dp[i][j] = 1;
                        return true;
                    }
                }
            }
        }
        dp[i][j] = 0;
        return false;
    }

    private boolean check(int i) {
        if (i == m) return true;
        if (p.charAt(i) == '*') ++i;
        while(i < m) {
            if (i + 1 == m) return false;
            if (p.charAt(i + 1) != '*') return false;
            i += 2;
        }
        return true;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
