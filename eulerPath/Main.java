// http://informatics.mccme.ru/mod/statements/view3.php?chapterid=1704#1
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    int n;
    List<Edge> []g;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        n = nextInt();

        g = new ArrayList[n + 1];
        for(int i = 0; i <= n; ++i) g[i] = new ArrayList<>();
        for(int i = 0; i < 2 * n; ++i) {
            int v = nextInt(), u = nextInt();
            int from = (v + 3) / 4, to = (u + 3) / 4;
            Edge e1 = new Edge(from, to, v, u);
            Edge e2 = new Edge(to, from, u, v);
            e1.rev = e2;
            e2.rev = e1;
            g[from].add(e1);
            g[to].add(e2);
        }

        boolean []used = new boolean[n + 1];

        Stack<Integer> stack = new Stack<>();
        stack.add(1);
        
        List<Integer> path = new LinkedList<Integer>();
        int []ptr = new int[n + 1];
        while(stack.size() > 0) {
            int v = stack.peek();
            used[v] = true;
            boolean found = false;
            for(;ptr[v] < g[v].size(); ++ptr[v]) {
                Edge edge = g[v].get(ptr[v]);
                if (edge.used) continue;
                edge.used = edge.rev.used = true;
                ++ptr[v];
                found = true;
                stack.add(edge.to);
                break;
            }
            if (!found) {
                path.add(stack.pop());
            }
        }

        for(int i = 1; i <= n; ++i) {
            if (!used[i]) {
                out.println("No");
                out.close();
                return;
            }
        }

        out.println("Yes");
        Collections.reverse(path);
        for(int i = 0; i + 1 < path.size(); ++i) {
            int from = path.get(i), to = path.get(i + 1);
            for(int j = 0; j < g[from].size(); ++j) {
                Edge edge = g[from].get(j);
                if (edge.to == to && edge.used) {
                    edge.used = edge.rev.used = false;
                    out.print(edge.gate1 + " " + edge.gate2 + " ");
                    break;
                }
            }
        }

        out.println();
        out.close();
    }

    class Edge {
        int from, to;
        int gate1, gate2;
        Edge rev;
        boolean used;

        public Edge(int from, int to, int gate1, int gate2) {
            this.from = from;
            this.to = to;
            this.rev = null;
            this.used = false;
            this.gate1 = gate1;
            this.gate2 = gate2;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}