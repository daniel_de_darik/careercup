// http://informatics.mccme.ru/mod/statements/view3.php?chapterid=1704#1
import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }

    int n;
    List<Integer> []g;
    List<Edge> e;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        n = nextInt();

        e = new ArrayList<>();
        g = new ArrayList[n + 1];
        for(int i = 0; i <= n; ++i) g[i] = new ArrayList<>();
        for(int i = 0; i < 2 * n; ++i) {
            int v = nextInt(), u = nextInt();
            int from = (v + 3) / 4, to = (u + 3) / 4;
            g[from].add(e.size());
            e.add(new Edge(from, to, v, u));
            g[to].add(e.size());
            e.add(new Edge(to, from, u, v));
        }

        boolean []used = new boolean[n + 1];
        List<Integer> path = new LinkedList<>();

        int []ptr = new int[n + 1];

        Stack<Integer> stack = new Stack<>();
        stack.add(1);

        while(stack.size() > 0) {
            int v = stack.peek();
            used[v] = true;
            boolean found = false;
            for(;ptr[v] < g[v].size(); ++ptr[v]) {
                int index = g[v].get(ptr[v]);
                Edge edge = e.get(index);
                if (edge.used) continue;
                edge.used = e.get(index ^ 1).used = true;
                stack.add(edge.to);
                found = true;
                ++ptr[v];
                break;
            }

            if (!found) {
                path.add(stack.pop());
            }
        }

        for(int i = 1; i <= n; ++i) {
            if (!used[i]) {
                out.println("No");
                out.close();
                return;
            }
        }

        out.println("Yes");
        Collections.reverse(path);
        for(int i = 0; i + 1 < path.size(); ++i) {
            int from = path.get(i), to = path.get(i + 1);
            for(int j = 0; j < g[from].size(); ++j) {
                int index = g[from].get(j);
                Edge edge = e.get(index);
                if (edge.to == to && edge.used) {
                    edge.used = e.get(index ^ 1).used = false;
                    out.print(edge.gate1 + " " + edge.gate2 + " ");
                    break;
                }
            }
        }

        out.println();
        out.close();
    }

    class Edge {
        int from, to;
        int gate1, gate2;
        boolean used;

        public Edge(int from, int to, int gate1, int gate2) {
            this.from = from;
            this.to = to;
            this.used = false;
            this.gate1 = gate1;
            this.gate2 = gate2;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Solver().run();
    }
}