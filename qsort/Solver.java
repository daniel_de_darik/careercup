import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++){
            a[i] = nextInt();
        }

        Arrays.sort(a);

        System.out.println(Arrays.toString(a));
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Solver().run();
    }
}