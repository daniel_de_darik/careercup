#/bin/bash

javac Main.java
javac Solver.java
javac gen.java

GREEN='\e[1;32m'
RED='\e[1;31m'
NOCOLOR='\e[0m'
DIFF=0
COUNT=0

while [ $DIFF -ne 1 ]; do
	java gen > in.txt
	java Solver <in.txt >out1.txt
	java Main <in.txt >out2.txt

	if diff out1.txt out2.txt >/dev/null; then
		echo -e "${GREEN}PASSED TEST $COUNT ${NC}"
	else
		echo -e "${RED}WRONG ANSWER $COUNT ${NC}"
		let DIFF=1
	fi

	let COUNT=COUNT+1
done