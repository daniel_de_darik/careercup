import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    int [][]dp, mem;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
    	int n = nextInt(), k = nextInt();
    	dp = new int[n+1][k+1];
    	for(int i=0;i<=n;i++){
    		Arrays.fill(dp[i], -1);
    	}
    	mem = new int[n+1][k+1];
    	for(int i=0;i<=n;i++){
    		Arrays.fill(mem[i], -1);
    	}
    	System.out.println(dp(n, k) + " " + dp1(n, k));
        out.close();	
    }

    private int dp(int n, int k) {
    	if (k == 1) return n;
    	if (2 * k - 1 > n) return 0;
    	if (dp[n][k] != -1) return dp[n][k];
    	int ret = 0;
    	for(int i=n;i>=2*k-1;i--){
    		ret+=dp(i-2, k - 1);
    	}
    	return dp[n][k] = ret;
    }

    private int dp1(int n, int k) {
    	if (k == 1) return n;
    	if (2 * k - 1 > n) return 0;
    	if (mem[n][k] != -1) return mem[n][k];
    	int ret = dp1(n-1, k) + dp1(n-2, k-1);
    	return mem[n][k] = ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}