import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    char [][]map;
    int n,m;
    boolean used[][];
    
    int di[] = new int[]{0, 0, 1, -1};
    int dj[] = new int[]{1, -1, 0, 0};
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	n = nextInt();
	m = nextInt();
	
	map = new char[n][m];
	
	for(int i=0;i<n;i++){
	    map[i] = next().toCharArray();
	}
	used = new boolean[n][m];
	
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
		if (!used[i][j] && map[i][j] == 'o'){
		    if (bfs(i, j)) fill(i, j);
		}
	    }
	}
	
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
		System.out.print(map[i][j]);
	    }
	    System.out.println();
	}
	
        out.close();	
    }
    
    Queue<int[]> q;
    
    private boolean bfs(int x, int y){
	q = new ArrayDeque<int[]>();
	q.add(new int[]{x, y});
	
	boolean valid = true;
	
	while(q.size() > 0){
	    int []cur = q.poll();
	    int a = cur[0], b = cur[1];
	    for(int i=0;i<di.length;i++){
		int ni = a + di[i];
		int nj = b + dj[i];
		
		if(ni >= 0 && ni < n && nj >=0 && nj < m){
		    if (!used[ni][nj] && map[ni][nj] == 'o'){
			used[ni][nj] = true;
			q.add(new int[]{ni, nj});
		    }
		}else{
		    valid = false;
		}
	    }
	}
	
	return valid;
    }
    
    private void fill(int x, int y){
	q = new ArrayDeque<int[]>();
	q.add(new int[]{x, y});
	
	while(q.size() > 0){
	    int []cur = q.poll();
	    int a = cur[0], b = cur[1];
	    for(int i=0;i<di.length;i++){
		int ni = a + di[i];
		int nj = b + dj[i];
		if (ni >=0 && ni < n && nj >= 0 && nj < m){
		    if (map[ni][nj] == 'o'){
			map[ni][nj] = 'x';
			q.add(new int[]{ni, nj});
		    }
		}
	    }
	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}