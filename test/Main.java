import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	Integer x = 1;
	out.println(x);
	modify(x);
	out.println(x);
        out.close();
    }
    
    public void modify(Integer a){
	a++;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}