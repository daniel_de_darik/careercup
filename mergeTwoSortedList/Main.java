import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        ListNode h1 = new ListNode(1);
        ListNode runner = h1;
        runner.next = new ListNode(3);
        runner = runner.next;
        runner.next = new ListNode(5);

        print(h1);

        ListNode h2 = new ListNode(2);
        runner = h2;
        runner.next = new ListNode(3);
        runner = runner.next;
        runner.next = new ListNode(6);

        print(h2);
        runner = mergeTwoLists(h1, h2);
        
        print(runner);
        out.close();
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        ListNode tail = new ListNode(0);
        ListNode ret = tail;
        while(l1 != null && l2 != null){
            if (l1.val > l2.val){
                tail.next = l2;
                l2 = l2.next;
            }else if (l1.val < l2.val){
                tail.next = l1;
                l1 = l1.next;
            }else{
                tail.next = l1;
                l1 = l1.next;
                tail = tail.next;
                tail.next = l2;
                l2 = l2.next;
            }
            tail = tail.next;
        }

        if (l1 != null) tail.next = l1;
        if (l2 != null) tail.next = l2;
        return ret.next;
    }

    private void print(ListNode head) {
        ListNode runner = head;
        while(runner != null) {
            System.out.println(runner);
            runner = runner.next;
        }
        System.out.println("-----------------------------");
    }

    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }

        public String toString() {
            return "" + val;
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
