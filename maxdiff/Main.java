import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	int n = nextInt();
	int []a = new int[n];
	int []b = new int[n];
	for(int i=0;i<n;i++){
	    a[i] = nextInt();
	    b[n-i-1] = a[i];
	}
	
	int [][]max = min(a);
	int [][]min = max(b);
	/*
	for(int i=0;i<2;i++){
	    for(int j=0;j<n;j++){
		System.out.print(max[j][i] + " ");
	    }
	    System.out.println();
	}
	
	System.out.println();
	for(int i=0;i<2;i++){
	    for(int j=0;j<n;j++){
		System.out.print(min[j][i] + " " );
	    }
	    System.out.println();
	}
	*/
	int ret = find(max(a), min(b));
	ret = Math.max(ret, find(min(a), max(b)));
	out.println(ret);
        out.close();
    }
    
    private int find(int [][]a, int [][]b){
	int n = a.length;
	int ret = -1;
	for(int i=0;i<n;i++){
	    int left = a[i][0];
	    int right = Math.min(b[n-i-1][0], b[n-i-1][1]);
	    
	    ret = Math.max(ret, Math.abs(left - right));
	    left = a[i][1];
	    right = b[n-i-1][0];
	    
	    ret = Math.max(ret, Math.abs(left - right));
	}
	
	return ret;
    }
    
    private int[][] max(int a[]){
	int n = a.length;
	int [][]dp = new int[n][2];
	dp[0][0] = 0;
	dp[0][1] = Math.max(a[0], 0);
	for(int i=1;i<n;i++){
	    dp[i][0] = Math.max(dp[i-1][1], dp[i-1][0]);
	    dp[i][1] = Math.max(a[i], dp[i-1][1] + a[i]);
	}
	
	return dp;
    }
    
    private int[][] min(int []a){
	int n = a.length;
	int [][]dp = new int[n][2];
	dp[0][0] = 0;
	dp[0][1] = Math.min(a[0], 0);
	
	for(int i=1;i<n;i++){
	    dp[i][0] = Math.min(dp[i-1][0], dp[i-1][1]);
	    dp[i][1] = Math.min(a[i], dp[i-1][1] + a[i]);
	}
	
	return dp;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}