import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
		long x = nextInt();
		System.out.println(toBinary(x));
		x = reverse(x);
		System.out.println(toBinary(x));
        out.close();
    }
    
    private String toBinary(long x){
		String r = "";
		for(int i=0;i<32;i++){
		    if ((x & (1L << i) ) > 0) r+="1";
		    else r+="0";
		}
		return r;
    }
    
    private long reverse(long x){
		long y = 0x55555555;
		x = ((x & y) << 1) | ((x >> 1) & y);
		y = 0x33333333;
		x = ((x & y) << 2) | ((x >> 2) & y);
		y = 0x0f0f0f0f;
		x = ((x & y) << 4) | ((x >> 4) & y);
		y = 0x00ff00ff;
		x = ((x & y) << 8) | ((x >> 8) & y);
		return (x << 16) | (x >> 16);
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}