import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
		int n = nextInt();
		int []a = new int[n];
		for(int i=0;i<n;i++) a[i] = nextInt();
		int min = a[0], max = a[0];
		for(int i=1;i<n;i++){
		    min = Math.min(min, a[i]);
		    max = Math.max(min, a[i]);
		}
		
		Arrays.sort(a);
		for(int i=0;i<n;i++){
		    a[i]-=min;
		}
		for(int i=0;i<10;i++){
		    int x = getRnd(min, max, a);
		    System.out.println(x);
		}
	
        out.close();
    }
    
    Random rnd = new Random();
    
    private int getRnd(int min, int max, int []a){
		int x = rnd.nextInt(max - min + 1 - a.length);
		//System.out.println("x="+x);
		int l = -1, r = a.length;
		
		while(r - l > 1){
		    int m = l + (r-l)/2;
		    if (a[m] - m - 1 < x){
				l = m;
		    }else{
				r = m;
		    }
		}
		
		int left = a[l] - l - 1;
		x-=left;
		
		return a[l] + x + min;
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}