import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
		Cache<Integer> cache = new Cache<Integer>(2);
		int m = nextInt();
		for(int i=0;i<m;i++){
		    String op = next();
		    int key = nextInt();
		    if (op.equals("GET")){
				System.out.println("GET " + key);
				System.out.println(cache.get(key));
		    }else{
				int value = nextInt();
				System.out.println("SET " + key + " " + value);
				cache.set(key, value);
		    }
		    cache.print();
		    System.out.println("---------------------------------------");
		}
	
        out.close();
    }
    
    static class Node<T>{
		int key;
		T value;
		long time;
		Node next, prev;
		
		public Node(int key, T value){
		    this.key = key;
		    this.value = value;
		    this.time = System.currentTimeMillis();
		}
		
		public void updateTime(){
		    this.time = System.currentTimeMillis();
		}
		
		@Override
		public String toString(){
		    return this.key + " " + this.value;
		}
    }
    
    static class Cache<T>{
		Node<T> head, tail;
		Map<Integer, Node<T>> map;
		int limit, size;
		
		public Cache(int limit){
		    map = new HashMap<Integer, Node<T>>();
		    this.limit = limit;
		    size = 0;
		}
		
		public T get(int key){
		    if (!map.containsKey(key)) return null;
		    Node<T> node = map.get(key);
		    update(key, node.value);
		    
		    return node.value;
		}
		
		public void set(int key, T value){
		    if (map.containsKey(key)){
				update(key, value);
				size++;
		    }else{
				Node<T> node = new Node<T>(key, value);
				if (size + 1 <= limit){
				    if (head == null){
						head = node;
				    }else{
						tail.next = node;
						node.prev = tail;
				    }
				    map.put(node.key, node);
				    tail = node;
				    size++;
				}else{
				    map.remove(head.key);
				    head = head.next;
				    if (head != null){
						head.prev = null;
				    }else{
						tail = null;
				    }
				    
				    if (tail != null){
						tail.next = node;
						node.prev = tail;
				    }else{
						head = node;
				    }
				    map.put(key, node);
				    tail = node;
				}
		    }
		}
		
		private void update(int key, T value){
		    Node<T> node = map.get(key);
		    node.value = value;
		    
		    if(tail.key == node.key){
				node.updateTime();
				return;
		    }
		    
		    Node<T> prev = node.prev;
		    Node<T> next = node.next;
		    
		    if (prev != null) {
		        prev.next = next;
		    }
		    if (next != null) {
		        next.prev = prev;
		    }
		    if (head.key == node.key){
				head = head.next;
		    }
		    tail.next = node;
		    node.prev = tail;
		    node.next = null;
		    node.updateTime();
		    
		    map.put(node.key, node);
		    tail = node;
		}
		
		public void print(){
		    System.out.println("Printing cache values");
		    Node<T> cur = head;
		    while(cur != null){
				System.out.println(cur);
				cur = cur.next;
		    }
		}
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}