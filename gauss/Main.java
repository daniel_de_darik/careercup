import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    private double nextDouble() throws Exception{
	return Double.parseDouble(next());
    }
    
    final static double EPS = 1e-9;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt(), m = nextInt();
	double [][]a = new double[n][m];
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
		a[i][j] = nextDouble();
	    }
	}
	
	int where[] = new int[m-1];
	Arrays.fill(where, -1);
	for(int row=0,col=0;row < n && col < m-1;col++){
	    int sel = row;
	    for(int i=row+1;i<n;i++){
		if(Math.abs(a[i][col]) > Math.abs(a[sel][col])){
		    sel = i;
		}
	    }
	    
	    if (Math.abs(a[sel][col]) < EPS) continue;
	    where[col] = row;
	    if (sel != row){
		for(int j=col;j<m;j++){
		    double tmp = a[row][j];
		    a[row][j] = a[sel][j];
		    a[sel][j] = tmp;
		}
	    }
	    
	    for(int i=0;i<n;i++){
		if (i == row) continue;
		double c = a[i][col]/a[row][col];
		for(int j=col;j<m;j++){
		    a[i][j]-=c * a[row][j];
		}
	    }
	    row++;
	}
	
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
		System.out.print(a[i][j] + " ");
	    }
	    System.out.println();
	}
	
	double ans[] = new double[m-1];
	for(int i=0;i<m-1;i++){
	    if (where[i] != -1){
		ans[i] = a[where[i]][m-1]/a[where[i]][i];
	    }
	}
	
	for(int i=0;i<n;i++){
	    double sum = 0;
	    for(int j=0;j<m-1;j++){
		sum+=ans[j] * a[i][j];
	    }
	    if (Math.abs(sum - a[i][m-1]) > EPS){
		System.out.println("No solution :(");
		return;
	    }
	}
	for(int i=0;i<m-1;i++){
	    if (where[i] == -1){
		System.out.println("Too many solutions :)");
		return;
	    }
	}
	
	System.out.println(Arrays.toString(ans));
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}