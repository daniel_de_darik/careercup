import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    final int oo = Integer.MAX_VALUE / 2;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []from = new int[n + 1];
        Arrays.fill(from, -1);
        int []dp = new int[n + 1];
        Arrays.fill(dp, oo);
        dp[0] = 0;
        for(int i = 1; i <= n; ++i) {
            for(int j = 1; j * j <= i; ++j) {
                if (dp[i] > dp[i - j * j] + 1) {
                    dp[i] = dp[i - j * j] + 1;
                    from[i] = j;
                }
            }
        }

        List<Integer> ans = new LinkedList<Integer>();
        while(n > 0) {
            ans.add(from[n]);
            n -= from[n] * from[n];
        }
        Collections.reverse(ans);
        out.println(ans.size());
        for(int i : ans) {
            out.print(i + " ");
        }
        out.println();

        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}