import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        int ret = 0, size = 1;
        for(int i = 0; i + 1 < n; ++i) {
            if (a[i + 1] - a[i] == 1) {
                ++size;
            }else {
                ret = Math.max(ret, size);
                size = 1;
            }
        }
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}