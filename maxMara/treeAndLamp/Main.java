/* Дан лес и фонарь, который может освещать угол alpha. найти место куда поставить
фонарь так, что он осветит макс количество деревьев и это количество*/
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    P []p;

	public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	int n = nextInt(), alpha = nextInt();
    	double angle = Math.PI * alpha / 180;
    	
    	p = new P[n];
    	for(int i = 0; i < n; ++i) p[i] = new P(nextInt(), nextInt());
    	Arrays.sort(p);

    	int max = 0;
    	for(int i = 0; i < n; ++i) {
    		for(int j = i + 1; j < n; ++j) {
    			int a = p[j].x - p[i].x, b = p[j].y - p[i].y;
    			int lo = j, hi = n;
    			while(hi - lo > 1) {
    				int m = lo + (hi - lo) / 2;
    				int a1 = p[m].x - p[i].x, b1 = p[m].y - p[i].y;
    				
    				double angle1 = new P(a1, b1).angle() - new P(a, b).angle();
    				if (angle1 <= angle) {
    					lo = m;
    				}else {
    					hi = m;
    				}
    			}
    			max = Math.max(max, lo - j + 2);
    		}
    	}
    	// int at = 0;
    	// int max1 = 0;
    	// for(int i = 0; i < n; ++i) {
    	// 	System.out.println("diff = " + (p[i].angle() - p[at].angle()));
    	// 	while(p[i].angle() - p[at].angle() > alpha) {
    	// 		++at;
    	// 	}
    	// 	System.out.println(i + " " + at);
    	// 	max1 = Math.max(max1, i - at + 1);
    	// }

    	out.println(max);
        out.close();
    }

    private double angle(int a1, int b1, int a2, int b2) {
    	double len1 = Math.hypot(a1, b1), len2 = Math.hypot(a2, b2);
    	double cos = (a1 * a2 + b1 * b1) / (len1 * len2);
    	return cos;
    }

    final static double EPS = 1e-9;

    class P implements Comparable<P> {
    	int x, y;
    	public P(int x, int y) {
    		this.x = x;
    		this.y = y;
    	}

    	public double angle() {
    		return Math.atan2(y, x);
    	}

    	public int compareTo(P other) {
    		double angle1 = this.angle();
    		double angle2 = other.angle();
    		if (Math.abs(angle1 - angle2) < EPS) return 0;
    		if (angle1 < angle2) return -1;
    		return 1;
    	}

    	public String toString() {
    		return "[x=" + x + ", y = " + y + ", alpha = " + this.angle() + "]";
    	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}