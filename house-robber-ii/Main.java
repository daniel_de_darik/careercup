import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	for(int i = 0; i < n; ++i) a[i] = nextInt();
	System.out.println(rob(a));
        out.close();
    }
    
    public int rob(int[] nums) {
        int max = 0, n = nums.length;
        if(n == 0) return 0;
        if (n == 1) return nums[0];
        if (n == 2) return Math.max(nums[0], nums[1]);
        /*
        for(int i = 0; i < n; ++i){
            int []a = new int[n];
            for(int j = i, ptr = 0; ptr < n; ++j) a[ptr++] = nums[j % n];
            max = Math.max(max, solve(a));
        }
        */
        return solve(nums);
    }

    private int solve(int a[]){
        int n = a.length;
        int []b = new int[n - 1];
        for(int i = 1, ptr = 0; i < n; ++i) b[ptr++] = a[i];
        int max = dp(b);
        b = new int[n - 3];
        for(int i = 2, ptr = 0; i < n - 1; ++i) b[ptr++] = a[i];

        return Math.max(max, dp(b) + a[0]);
    }

    private int dp(int []a){
        int n = a.length;
        int [][]dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = a[0];
        for(int i = 1; i < n; ++i){
            dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1]);
            dp[i][1] = dp[i-1][0] + a[i];
        }

        return Math.max(dp[n-1][0], dp[n-1][1]);
    }
    
    public int rob1(int[] nums) {
        int le = nums.length;
        if(le==0) return 0;
        else if (le==1) return nums[0];
        else if (le==2) return Math.max(nums[0],nums[1]);
        //Create an array to store the sums
        int[] arr = new int[le];
        arr[0] = nums[0];
        arr[1] = nums[1];
        // Exclude the last value in the nums array, and calculate the sum just like you do if the houses are in linear order
        for(int i=2;i<le-1;i++) {
            if (i>2) arr[i] = Math.max(arr[i-2],arr[i-3]) + nums[i];
            else arr[i] = arr[i-2] + nums[i];
        }
        // Find the maximum excluding the last house
        int maxWithOutLast = Math.max(arr[le-2],arr[le-3]);
        arr[2] = nums[2];
        // Exclude the first value in the nums array, and calculate the sum just like you do if the houses are in linear order
        for(int i=3;i<le;i++) {
            if (i>3) arr[i] = Math.max(arr[i-2],arr[i-3]) + nums[i];
            else arr[i] = arr[i-2] + nums[i];
        }
        // Find the maximum excluding the first house
        int maxWithOutFirst = Math.max(arr[le-2],arr[le-1]);
        // Return the maximum of the two 
        return Math.max(maxWithOutLast,maxWithOutFirst);
    }
    
    private void stress(){
	Random rnd = new Random();
	int cnt = 0;
	while(cnt++ < 10000){
	    int n = rnd.nextInt(10);
	    int []a = new int[n];
	    for(int i = 0; i < n; ++i) a[i] = rnd.nextInt(10000);
	    int me = rob(a), he = rob1(a);
	    if (me != he){
		System.out.println("Expected " + me + " Received " + he);
		System.out.println(Arrays.toString(a));
	    }else{
		System.out.println("ACCEPTED " + cnt);
	    }
	}
	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
	   //new Main().stress();
    }
}