import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	char []a = next().toCharArray();
	int w = 0;
	for(int i=0;i<a.length;i++) if (a[i] == '?') w++;
	int wild[] = new int[w];
	
	int c = 0;
	while(c++ < 10){
	    String x = nextGen(a, wild);
	    System.out.println(x);
	}
	
        out.close();
    }
    
    private String nextGen(char []x, int []w){
	String ret = "";
	int yk = 0;
	for(int i=0;i<x.length;i++){
	    if (x[i] == '?'){
		int c = w[yk];
		String s = "";
		if (c == 0) s = "0";
		else{
		    while(c > 0){
			s=Integer.toString(c % 2) + s;
			c/=2;
		    }
		}
		ret+=s;
		w[yk++]++;
	    }else{
		ret+=x[i];
	    }
	}
	
	return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}