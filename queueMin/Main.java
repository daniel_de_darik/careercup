import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	

        out.close();
    }

    class MyQueue {
    	Stack<P> s1, s2;
    	public MyQueue() {
    		s1 = new Stack<P>();
    		s2 = new Stack<P>();
    	}

    	public void add(int x) {
    		int min = x;
    		if (s1.size() > 0) {
    			min = Math.min(x, s1.peek().min);
    		}
    		s1.push(new P(x, min));
    	}

    	public int poll() {
    		if (s2.size() == 0) {
    			move();
    		}
    		if (s2.size() == 0) throw new RuntimeException("No elements in Queue");
    		return s2.pop().v;
    	}

    	public int min() {
    		if (s1.size() == 0 || s2.size() == 0) {
    			if (s1.size() == 0 && s2.size() == 0) throw new RuntimeException("No elements in Queue");
    			if (s1.size() == 0) return s2.peek().min;
    			return s1.peek().min; 
    		}else {
    			return Math.min(s1.peek().min, s2.peek().min);
    		}
    	}

    	private void move() {
    		while(s1.size() > 0) {
    			P cur = s1.pop();
    			int curmin = cur.min;
    			if (s2.size() > 0) curmin = Math.min(curmin, s2.peek().min);
    			s2.push(new P(cur.v, curmin));
    		}
    	}
    }

    class P {
    	int v, min;
    	public P(int v, int min) {
    		this.v = v;
    		this.min = min;
    	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
