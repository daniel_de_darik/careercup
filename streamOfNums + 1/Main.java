import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = nextInt();
	
	int yk = 0;
	int prev = -1, cnt = 0;
	while(yk < n){
	    int num = a[yk];
	    if (num != 9){
		if (prev > 0){
		    System.out.print(prev);
		}
		for(int i=0;i<cnt;i++){
		    System.out.print(9);
		}
		prev = num;
		cnt = 0;
	    }else{
		cnt++;
	    }
	    yk++;
	}
	
	if (prev == -1){
	    System.out.print(1);
	}else{
	    System.out.print(prev+1);
	}
	for(int i=0;i<cnt;i++){
	    System.out.print(0);
	}
	System.out.println();
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}