import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    final static int BASE=10;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
		int n = nextInt();
		int []a = new int[n];
		for(int i=0;i<n;i++) a[i] = nextInt();
		radixSort(a);
		System.out.println(Arrays.toString(a));
        out.close();
    }
    
    private void radixSort(int[] a){
		int n = a.length;
		int max = a[0];
		for(int i=1;i<n;i++) max = Math.max(max, a[i]);
		List<Integer>[]bucket = new LinkedList[BASE];
		for(int i=0;i<BASE;i++) bucket[i] = new LinkedList<Integer>();
		
		for(int i=1;i<=max;i*=BASE){
		    for(int j=0;j<n;j++){
				bucket[(a[j] / i) % BASE].add(a[j]);
		    }
		    int at = 0;
		    for(int j=0;j<BASE;j++){
				for(int elm : bucket[j]){
				    a[at++] = elm;
				}
				bucket[j].clear();
		    }
		}
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}