import java.util.*;
public class Solver{

    Random rnd;

    public void run(){
	Scanner in = new Scanner(System.in);
	int n = in.nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = in.nextInt();
	rnd = new Random();
	sort(0, n-1, a);
	System.out.println(Arrays.toString(a));
    }
    
    private void sort(int l, int r, int []a){
	int m = l + rnd.nextInt(r - l + 1);
	int x = a[m];
	int i = l, j = r;
	while(i <= j){
	    while(a[i] < x) i++;
	    while(a[j] > x) j--;
	    if (i <= j){
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
		i++;
		j--;
	    }
	}
	
	if (l < j) sort(l, j, a);
	if (i < r) sort(i, r, a);
    }

    public static void main(String args[]){
	new Solver().run();
    }
}