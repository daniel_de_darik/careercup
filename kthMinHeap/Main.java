import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
		return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
		Heap<Integer> min = new Heap(100, new Comparator<Integer>(){
		    public int compare(Integer a, Integer b){
				return a - b;
		    }
		});
		
		Heap<Pair> min1 = new Heap(100, new Comparator<Pair>(){
		    public int compare(Pair a, Pair b){
				return a.data - b.data;
		    }
		});
		
		min.add(9);
		min.add(3);
		min.add(4);
		min.add(7);
		min.add(6);
		min.add(1);
		min.add(3);
		
		while(min.size() > 0){
		    out.println(min.poll());
		}
		out.println();
		min.add(9);
		min.add(3);
		min.add(4);
		min.add(7);
		min.add(6);
		min.add(1);
		min.add(3);
		
		int k = 3;
		min1.add(new Pair(min.getById(1), 1));
		for(int i=0;i<k-1;i++){
		    Pair cur = min1.poll();
		    int idx = cur.idx;
		    Integer left = min.getById(2 * idx);
		    if (left != null) min1.add(new Pair(min.getById(2 * idx), 2 * idx));
		    Integer right = min.getById(2 * idx + 1);
		    if (right != null) min1.add(new Pair(min.getById(2 * idx + 1), 2 * idx + 1));
		}
	
		out.println(min1.peek());
        out.close();
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}

class Pair{
    int data, idx;
    public Pair(int data, int idx){
		this.idx = idx;
		this.data = data;
    }
    
    public String toString(){
		return "" + this.data;
    }
}

class Heap<T>{
    T []a;
    int size;
    Comparator<T> cmp;
    public Heap(int n, Comparator<T> cmp){
		a = (T[])new Object[n];
		size = 1;
		this.cmp = cmp;
    }
    
    public T peek(){
		if (size == 1) return null;
		return a[1];
    }
    
    public void add(T data){
		a[size] = data;
		siftUp(size);
		size++;
    }
    
    private void siftUp(int i){
		while(i > 1 && cmp.compare(a[i/2], a[i]) > 0){
		    T t = a[i/2];
		    a[i/2] = a[i];
		    a[i] = t;
		    i/=2;
		}
    }
    
    public T poll(){
		T min = a[1];
		a[1] = a[size-1];
		size--;
		siftDown(1);
		
		return min;
    }
    
    private void siftDown(int i){
		while(2 * i < size){
		    int j = i;
		    if (cmp.compare(a[2*i], a[j]) < 0) j = 2 * i;
		    if (2 * i + 1 < size && cmp.compare(a[2*i+1], a[j]) < 0) j = 2 * i + 1;
		    
		    if (i == j) break;
		    T t = a[i];
		    a[i] = a[j];
		    a[j] = t;
		}
    }
    
    public T getById(int i){
		if (i >= size) return null;
		return a[i];
    }
    
    public int size(){
		return size - 1;
    }
}