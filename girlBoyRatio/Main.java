import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
    	
    	double sum = 1;
    	for(int i = 2; i < 1000000; ++i) sum += 1. / i;
    	out.println(sum);
    	
	double x = 0.5;
	double p = 1, s = 0;
	for(int i = 1; i < 1000000; ++i){
	    s += p * i;
	    p *= x;
	}
	
	out.println(s / 4);
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}