import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt(), k = nextInt();
	int []a = new int[n];
	for(int i=0;i<n;i++) a[i] = i;
	k%=n;
	if (k == 0){
	    System.out.println(Arrays.toString(a));
	    return;
	}
	for(int i=0;i<n;i++){
	    if (a[i] == i){
		int buffer = a[i], next = (i + k) % n;
		while(a[next] == next){
		    int t = a[next];
		    a[next] = buffer;
		    buffer = t;
		    next = (next + k) % n;
		}
	    }
	}
	System.out.println(Arrays.toString(a));
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}