import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	char []a = next().toCharArray();
	System.out.println(new String(a));
	int n = a.length, yk = 0;
	for(int i=0;i<n;i++){
	    if (a[i] != '_'){
		a[yk++] = a[i];
	    }else{
		if (yk - 1 >= 0 && a[yk-1] != '_'){
		    a[yk++] = '_';
		}
	    }
	}
	for(int i=yk;i<n;i++) a[yk++] = '_';
	
	System.out.println(new String(a));
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}