import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    Map<Character, Integer> map;
    Set<String> dict;
    
    V trie[];
    int size = 0;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
		int n = nextInt();
		
		map = new HashMap<Character, Integer>();
		char []a = next().toCharArray();
		for(int i=0;i<a.length;i++){
		    if (!map.containsKey(a[i])){
				map.put(a[i], 0);
		    }
		    int c = map.get(a[i]);
		    map.put(a[i], c + 1);
		}
		
		trie = new V[1000];
		trie[size++] = new V();
		for(int i=0;i<n;i++){
		    addString(next());
		}
		StringBuilder s = new StringBuilder();
		dfs(s);
	
        out.close();
    }
    
    private void addString(String s){
		int v = 0;
		for(int i=0;i<s.length();i++){
		    char c = s.charAt(i);
		    if (!trie[v].hasNext(c)){
				trie[v].addNext(c, size);
				trie[size++] = new V();
		    }
		    v = trie[v].getNext(c);
		}
		trie[v].leaf = true;
    }
    
    private void dfs(StringBuilder prefix){
		String s = prefix.toString();
		if (!isPrefix(s)) return;
		if (isWord(s)){
		    System.out.println(s);
		}
		for(Character c : map.keySet()){
		    int x = map.get(c);
		    if (x > 0){
				map.put(c, x-1);
				prefix = new StringBuilder(s);
				prefix.append(c);
				dfs(prefix);
				map.put(c, x);
		    }
		}
    }
    
    private boolean isPrefix(String s){
		int v = 0;
		for(int i=0;i<s.length();i++){
		    char c = s.charAt(i);
		    if (!trie[v].hasNext(c)) return false;
		    v = trie[v].getNext(c);
		}
		return true;
    }
    
    private boolean isWord(String s){
		int v = 0;
		for(int i=0;i<s.length();i++){
		    char c = s.charAt(i);
		    v = trie[v].getNext(c);
		}
		return trie[v].leaf;
    }
    
    static class V{
		Map<Character, Integer> map;
		boolean leaf;
		public V(){
		    map = new HashMap<Character, Integer>();
		    leaf = false;
		}
		
		public boolean hasNext(char c){
		    return map.containsKey(c);
		}
		
		public int getNext(char c){
		    return map.get(c);
		}
		
		public void addNext(char c, int x){
		    map.put(c, x);
		}
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}