import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt(), k = nextInt();
        // System.out.println(n + " " + k);
        int []a = new int[n];
        for(int i=0;i<n;i++){
            a[i] = nextInt();
        }

        // System.out.println(Arrays.toString(a));
        // swap(a, 0, 1);
        // System.out.println(Arrays.toString(a));

        int x = kth(0, n-1, a, k);
        // System.out.println(Arrays.toString(a));
        System.out.println(x);

        out.close();	
    }

    Random rnd = new Random();

    // private <T> void swap(T[] a, int i, int j) {

    //     T t = a[i];
    //     a[i] = a[j];
    //     a[j] = t;

    // } 
    private int kth(int l, int r, int[] a, int k) {
        int m = l + rnd.nextInt(r - l + 1);
        int x = a[m], i = l, j = r;
        // System.out.println(l + " " + r + " " + x);
        while(i <= j) {
            while(a[i] < x) i++;
            while(a[j] > x) j--;

            if (i <= j) {
                // a[i] = a[j] - a[i];
                // a[j] = a[j] - a[i];
                // a[i] = a[i] + a[j];
                // a[i] = a[i]^a[j];
                // a[j] = a[i]^a[j];
                // a[i] = a[i]^a[j];
                int t = a[i];
                a[i] = a[j];
                a[j] = t;
                i++;j--;
            }
        }

        if (l <= k && k <= j)
            return kth(l, j, a, k);

        if (i <= k && k <= r)
            return kth(i, r, a, k);

        return a[k];
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}