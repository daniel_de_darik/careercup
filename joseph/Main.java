import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt(), k = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();
        int ret = 0;
        for(int i=1;i<=n;i++){
            ret = (ret + k) % i;
        }
        out.println(a[ret]);
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}