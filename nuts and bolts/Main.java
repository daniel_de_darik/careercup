import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	int []a = new int[n];
	int []b = new int[n];
	for(int i = 0; i < n; ++i) a[i] = nextInt();
	for(int i = 0; i < n; ++i) b[i] = nextInt();
	
	sort(0, n-1, a, b);
	
	System.out.println(Arrays.toString(a));
	System.out.println(Arrays.toString(b));
	
        out.close();
    }
    
    private void sort(int l, int r, int []a, int []b){
	int m = (l + r) / 2;
	int pivot = a[m];
	int i = l, j = r, idx = -1;
	while(i <= j){
	    while(b[i] < pivot) i++;
	    while(b[j] > pivot) j--;
	    if (i <= j){
		int t = b[i];
		b[i] = b[j];
		b[j] = t;
		if (b[i] == pivot) idx = i;
		if (b[i] == pivot) idx = j;
		i++;
		j--;
	    }
	}
	
	System.out.println("pivot is " + pivot + " index is " + m);
	System.out.println(Arrays.toString(a));
	System.out.println(Arrays.toString(b));
	
	int i1 = i, j1 = j;
	if (idx == -1){
	    for(int k = l; k <= r; ++k) if (b[k] == pivot) idx = k;
	}
	//System.out.println(l + " " + r + " " + idx);
	pivot = b[idx];
	i = l;
	j = r;
	while(i <= j){
	    while(a[i] < pivot) i++;
	    while(a[j] > pivot) j--;
	    if (i <= j){
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
		i++;
		j--;
	    }
	}
	
	j = Math.max(j, j1);
	i = Math.min(i, i1);
	
	if (l < j) sort(l, j, a, b);
	if (i < r) sort(i, r, a, b);
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}