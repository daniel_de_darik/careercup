import java.util.*;
public class Tupoi{

    public void run(){
	Scanner in = new Scanner(System.in);
	int n = in.nextInt();
	int []a = new int[n];
	int []b = new int[n];
	for(int i = 0; i <n; ++i) a[i] = in.nextInt();
	for(int i = 0; i <n; ++i) b[i] = in.nextInt();
	
	Arrays.sort(a);
	Arrays.sort(b);
	System.out.println(Arrays.toString(a));
	System.out.println(Arrays.toString(b));
    }

    public static void main(String arsg[]){
	new Tupoi().run();
    }
}