import java.util.*;

public class gen{

    Random rnd;

    public void run(){
	rnd = new Random();
	int n = rnd.nextInt(7);
	if (n == 0) n = 2;
	System.out.println(n);
	int []a = new int[n];
	for(int i = 0; i < n; ++i) a[i] = rnd.nextInt(20);
	for(int i = 0; i < n; ++i) System.out.print(a[i] + " ");
	System.out.println();
	int []b = a.clone();
	shuffle(b);
	//System.out.println(Arrays.toString(a));
	//System.out.println(Arrays.toString(b));
	for(int i = 0; i < n; ++i) System.out.print(b[i] + " ");
	System.out.println();
	
    }
    
    private void shuffle(int a[]){
	int n = a.length;
	for(int i = 0; i < n; ++i){
	    int idx = rnd.nextInt(n - i) + i;
	    int t = a[i];
	    a[i] = a[idx];
	    a[idx] = t;
	}
    }

    public static void main(String args[]){
	new gen().run();
    }
}