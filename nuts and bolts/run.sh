#!/bin/bash

javac Main.java
javac gen.java
javac Tupoi.java

COUNT=0

while (( $COUNT <= 100 )); do
    java gen >in.txt
    java Main <in.txt >out1.txt
    java Tupoi <in.txt >out2.txt
    
    diff out1.txt out2.txt > /dev/null
    
    if (( "$?" == 1 ));then
	echo "WRONG ANSWER $COUNT"
	break
    else
	echo "PASSED $COUNT"
    fi
    
    COUNT=$[$COUNT+1]
done