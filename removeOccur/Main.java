import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        char []a = next().toCharArray();
        char []b = next().toCharArray();
        if (a.length < b.length) return;
        int yka = 0, ykb = 0;
        for(int i=0;i<a.length;i++){
            if (a[i] == b[ykb]) {
                ykb++;
                a[yka++] = a[i];
                if (ykb == b.length) {
                    yka-=ykb;
                    ykb = 0;
                }
            }else {
                if (ykb > 0) {
                    i-=ykb;
                    yka-=ykb-1;    
                }
                ykb = 0;
                // a[yka++] = a[i];
            }
        }
        System.out.println(new String(a, 0, yka));
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}