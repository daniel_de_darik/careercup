//clone linkedlist
//http://www.careercup.com/question?id=5412018236424192
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

    	Node a = new Node(1);
    	Node b = new Node(2);
    	Node c = new Node(3);
    	Node d = new Node(4);

    	a.next = b;
    	a.random = c;
    	b.next = c;
    	b.random = a;
    	c.next = d;
    	c.random = d;
    	d.random = d;

    	Map<Node, Node> map = new HashMap<Node, Node>();
    	Node cur = a;
    	Node cur1 = null, copy = null;

    	while(cur != null) {
    		Node x = new Node(cur.val);
    		if (copy == null) {
    			cur1 = x;
    			copy = x;
    		}else {
    			copy.next = x;
    			copy = copy.next;
    		}
    		map.put(cur, x);
    		cur = cur.next;
    	}

    	cur = a;
    	Node head = cur1;
    	while(cur != null) {
    		Node x = map.get(cur.random);
    		head.random = x;
    		cur = cur.next;
    		head = head.next;
    	}

    	cur = a;
    	while(cur != null) {
    		System.out.println(cur + "->" + cur.random);
    		cur = cur.next;
    	}

    	cur = cur1;
    	while(cur != null) {
    		System.out.println(cur + "->" + cur.random);
    		cur = cur.next;
    	}

        out.close();	
    }

    class Node {
    	Node next, random;
    	int val;

    	public Node(int val) {
    		this.val = val;
    	}

    	public int hashCode() {
    		return this.val;
    	}

    	public boolean equals(Object obj) {
    		Node other = (Node)obj;
    		return this.val == other.val;
    	}

    	public String toString() {
    		return ""+this.val;
    	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}