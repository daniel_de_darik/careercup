import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        String s = next();
        System.out.println(isValid(s));
        out.close();
    }

    public boolean isValid(String s) {
        char []a = new char[s.length()];
        int at = 0;
        for(int i = 0; i < s.length(); ++i){
            if (open(s.charAt(i))){
                a[at++] = s.charAt(i);
            }else{
                if (at == 0 || s.charAt(i) != get(a[at - 1])) return false;
                --at;
            }
        }
        return at == 0;
    }

    private char get(char x) {
        if (x == '(') return ')';
        if (x == '{') return '}';
        if (x == '[') return ']';   
        return ' ';
    }
    
    private boolean open(char x){
        return "({[".indexOf(x) >= 0;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
