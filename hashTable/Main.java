import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        MyHashTable ht = new MyHashTable(2);
        ht.add("abc", 1);
        ht.add("bc", 2);
        ht.add("bce", 3);
        ht.add("uid", 4);
        ht.add("sdf", 5);
        ht.add("aaa", 6);
        ht.add("acc", 7);

        System.out.println(ht.find("uid"));
        System.out.println(ht.find("aa"));
        System.out.println(ht.find("bc"));
        
        System.out.println(ht.size());
        System.out.println(ht.find("aaa"));
        ht.remove("aaa");
        System.out.println(ht.find("aaa"));

        System.out.println(ht.size());

        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}

class Pair {
    String key;
    int value;
    public Pair(String key, int value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for(int i=0;i<key.length();i++){
            hash = (127 * hash + key.charAt(i)) % 16908799;
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != this.getClass()) return false;
        Pair p = (Pair)obj;
        return p.key.equals(key);
    }

    @Override
    public String toString() {
        return key + " " + value;
    }
}
class MyHashTable{
    List<Pair> table[];
    int size, n;
    public MyHashTable(int n){
        this.n = n;
        table = new LinkedList[n];
        for(int i=0;i<n;i++) table[i] = new LinkedList<Pair>();
    }

    public Pair add(String key, int value) {
        Pair p = new Pair(key, value);
        int hash = p.hashCode();
        hash = compress(hash);
        table[hash].add(p);

        size++;
        if (size * 2 > n) {
            resize(2 * n);
        }

        return p;
    }

    public Pair find(String key) {
        Pair p = new Pair(key, -1);
        int hash = p.hashCode();
        hash = compress(hash);

        for(Pair pr : table[hash])
            if (pr.key.equals(key)) return pr;
        return null;
    }

    public void remove(String key) {
        Pair p = new Pair(key, -1);
        int hash = p.hashCode();
        hash = compress(hash);
        size--;
        table[hash].remove(p);
    }

    public int size() {
        return size;
    }

    private void resize(int ns) {
        System.out.println("Resizing...");
        List<Pair> []t = new LinkedList[ns];
        for(int i=0;i<ns;i++) t[i] = new LinkedList<Pair>();
        n = ns;
        for(int i=0;i<table.length;i++){
            for(Pair p : table[i]) {
                int hash = p.hashCode();
                hash = compress(hash);
                t[hash].add(p);
            }
        }

        table = t;
    }

    private int compress(int hash) {
        return (int)(((74343108L * hash + 974329L) % 674506111) % n);
    }
}