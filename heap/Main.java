import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

	MyHeap heap = new MyHeap();
	heap.add(3);
	heap.add(2);
	heap.add(20);
	heap.add(50);
	heap.add(50);
	
	out.println("peak heap = " + heap.peek());
	while(heap.size() > 0)
	    out.println(heap.poll());
	
	//out.println(heap.poll());
        out.close();	
    }
    
    static class MyHeap{
	
	final static int MAXN = 10000;
	int a[];
	int size;
	
	public MyHeap(){
	    a = new int[MAXN];
	    size = 1;
	}
	
	public int peek() throws Exception{
	    if (size() == 0)
		throw new Exception("No elements in heap exception :(");
	    return a[1];
	}
	
	public int poll() throws Exception{
	    if (size() == 0)
		throw new Exception("No elements in heap exception :(");
	    int min = a[1];
	    a[1] = a[size-1];
	    size--;
	    
	    siftDown(1);
	    
	    return min;
	}
	
	public void add(int x){
	    a[size] = x;
	    siftUp(size);
	    size++;
	}
	
	public int size(){
	    return size-1;
	}
	
	private void siftUp(int i){
	    while(i > 1 && a[i/2] > a[i]){
		int t = a[i];
		a[i] = a[i/2];
		a[i/2] = t;
		i/=2;
	    }
	}
	
	private void siftDown(int i){
	    while(2*i < size){
		int j = i;
		if (a[2*i] < a[j]) j = 2*i;
		if (2*i+1 < size && a[2*i+1] < a[j]) j = 2*i+1;
		if (i == j) break;
		
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
		i = j;
	    }
	}
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}