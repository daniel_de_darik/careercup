import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        MyQueue q = new MyQueue();
        q.add(1);
        q.add(2);
        q.add(3);
        q.add(4);
        q.add(5);
        q.add(6);
        q.add(7);

        while(q.size() > 0) {
            System.out.println(q.poll());
        }
        System.out.println();

        int n = nextInt();
        Stack<Integer> a = new Stack<Integer>();
        for(int i=0;i<n;i++) a.push(nextInt());
        Stack<Integer> res = sort(a);
        while(res.size() > 0)
            System.out.println(res.pop());


        out.close();	
    }

    private Stack<Integer> sort(Stack<Integer> a) {
        Stack<Integer> res = new Stack<Integer>();
        while(a.size() > 0) {
            int x = a.pop();
            while(res.size() > 0 && res.peek() < x) {
                a.push(res.pop());
            }
            res.push(x);
        }
        return res;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}

class MyQueue{
    Stack<Integer> stack1, stack2;

    public MyQueue() {
        stack1 = new Stack<Integer>();
        stack2 = new Stack<Integer>();
    }

    public void add(int x) {
        stack1.push(x);
    }

    public int poll() throws Exception{
        if (stack1.size() + stack2.size() == 0)
            throw new Exception("Empty queue exception :(");
        if (stack2.size() > 0) return stack2.pop();
        move();
        return stack2.pop();
    }

    public int peek() throws Exception {
        if (stack1.size() + stack2.size() == 0)
            throw new Exception("Empty queue exception :(");
        if (stack2.size() > 0) return stack2.peek();
        move();
        return stack2.peek();
    }

    public int size() {
        return stack1.size() + stack2.size();
    }

    private void move() {
        while (stack1.size() > 0) stack2.push(stack1.pop());
    }
}