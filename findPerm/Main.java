import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	char []a = next().toCharArray();
	int n = a.length;
	int i = 0, border = 1;
	
	if (a[0] == 'D'){
	    int ds = 0;
	    while(i < n && a[i] == 'D'){
		ds++;
		i++;
	    }
	    
	    int start = border + ds;
	    for(int c=0;c<ds+1;c++){
		System.out.print((start--) + " ");
	    }
	    border = border + ds + 1;
	}
	
	while(i < n){
	    int j = i, is = 0;
	    while(j < n && a[j] == 'I'){
		j++;is++;
	    }
	    int ds = 0;
	    while(j < n && a[j] == 'D'){
		j++;ds++;
	    }
	    int start = border + ds;
	    if (i == 0) is++;
	    for(int c=0;c<is;c++){
		System.out.print((start++) + " ");
	    }
	    start = border + ds - 1;
	    for(int c=0;c<ds;c++){
		System.out.print((start--) + " ");
	    }
	    
	    border = border + ds + is;
	    i = j;
	}
	System.out.println();
        out.close();
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}