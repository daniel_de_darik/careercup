import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        String s1 = next(), s2 = next(), s3 = next();

        System.out.println(isInterleave(s1, s2, s3));

        out.close();
    }

    int [][]dp;
    String s1, s2, s3;
    int n, m, k;
    public boolean isInterleave(String s1, String s2, String s3) {
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        n = s1.length();
        m = s2.length();
        k = s3.length();
        if (n + m != k) return false;
        if (n == 0 && m == 0 && k == 0) return true;
        if (n == 0) return s2.equals(s3);
        if (m == 0) return s1.equals(s3);
        dp = new int[n][m];
        for(int []i : dp) Arrays.fill(i, -1);
        return dp(0, 0);
    }
    
    private boolean dp(int i, int j){
        if (i == n){
            boolean good = true;
            for(int ptr = j; ptr < m; ++ptr){
                good &= s2.charAt(ptr) == s3.charAt(i + ptr);
            }
            return good;
        }
        if (j == m){
            boolean good = true;
            for(int ptr = i; ptr < n; ++ptr){
                good &= s1.charAt(ptr) == s3.charAt(j + ptr);
            }
            return good;
        }
        if (dp[i][j] != -1) return dp[i][j] == 1;
        int pos = i + j;
        if (s3.charAt(pos) == s1.charAt(i)) {
            if (dp(i + 1, j)){
                dp[i][j] = 1;
                return true;
            }
        }
        if (s3.charAt(pos) == s2.charAt(j)){
            if (dp(i, j + 1)){
                dp[i][j] = 1;
                return true;
            }
        }
        dp[i][j] = 0;
        return false;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
