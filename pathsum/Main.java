import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);

        System.out.println(hasPathSum(root, 1));

        out.close();
    }

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        return dfs(root, sum);
    }
    
    private boolean dfs(TreeNode root, int sum){
        if (sum < 0) return false;
        if (root == null) return sum == 0;
        if (dfs(root.left, sum - root.val)) return true;
        if (dfs(root.right, sum - root.val)) return true;
        return false;
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }


    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
