import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        Comparator<Integer> DESC = new Comparator<Integer>() {
            public int compare(Integer a, Integer b) {
                return b - a;
            }
        };

        PriorityQueue<Integer> min = new PriorityQueue<Integer>();
        PriorityQueue<Integer> max = new PriorityQueue<Integer>(10, DESC);

        int n = nextInt();
        for(int i=0;i<n;i++){
            String op = next();
            if (op.equals("ADD")) {
                int x = nextInt();
                if (min.size() > 0) {
                    int head = min.peek();
                    if (head < x) {
                        min.add(x);
                    }else {
                        max.add(x);
                    }    
                }else {
                    min.add(x);
                }
                
            }else {
                int res = -1;
                if ((min.size() + max.size()) % 2 == 0) {
                    res = (min.peek() + max.peek())/2;
                }else {
                    if (min.size() > max.size()) {
                        res = min.peek();
                    }else {
                        res = max.peek();
                    }
                }
                out.println(res);
            }

            if (min.size() > max.size() + 1) {
                max.add(min.poll());
            }else if (max.size() > min.size() + 1) {
                min.add(max.poll());
            }
        }

        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}