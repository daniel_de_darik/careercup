#/bin/bash

javac Main.java
javac Solver.java
javac gen.java

DIFF=0
COUNT=0

while [ $DIFF -ne 1 ]; do
    
    java gen > in.txt
    java Main < in.txt > out1.txt
    java Solver <in.txt > out2.txt
    
    if diff out1.txt out2.txt > /dev/null; then
	echo "PASSED TEST $COUNT"
    else
	echo "WRONG ANSWER on $COUNT"
	let DIFF=1
    fi
    
    let COUNT=COUNT+1
done