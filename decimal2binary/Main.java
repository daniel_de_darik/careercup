import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	//String n = next();
	//System.out.println(convert(n));
	
        out.close();
    }
    
    private String convert(String n){
		int w = Integer.parseInt(n.substring(0, n.indexOf(".")));
		double d = Double.parseDouble(n.substring(n.indexOf(".")));
		
		StringBuilder intPart = new StringBuilder();
		while(w > 0){
		    if (w % 2 == 0) intPart.append("0");
		    else intPart.append("1");
		    w/=2;
		}
		
		StringBuilder doublePart = new StringBuilder();
		while(d > 0){
		    if (doublePart.toString().length() > 32) return "Impossible to express";
		    d*=2;
		    if (d >= 1){
				doublePart.append("1");
				d-=1;
		    }else{
				doublePart.append("0");
		    }
		}
		
		return intPart.reverse() + "." + doublePart;
    }

    public static void main(String args[]) throws Exception{
		new Main().run();
    }
}