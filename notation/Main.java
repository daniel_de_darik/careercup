import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        String s = next();
        if (s == "" || s == null)
            throw new Exception("Invalid input Exception :(");

        out.println(find(s.toCharArray()));
        out.close();	
    }

    private int find(char []a) throws Exception{
        Stack<Integer> num = new Stack<Integer>();
        Stack<Character> op = new Stack<Character>();
        int n = a.length;

        for(int i=0;i<n;i++){
            if (isSpace(a[i])) continue;
            if (a[i] == '(') {
                op.push(a[i]);
            }else if (a[i] == ')') {
                while(!op.isEmpty() && op.peek() != '(') {
                    process(num, op.pop());
                }
                op.pop();
            }else if (isOperation(a[i])) {
                int p = priority(a[i]);
                while(!op.isEmpty() && priority(op.peek()) >= p) {
                    process(num, op.pop());
                }
                op.push(a[i]);
            }else {
                String k = "";
                while(i < n && isKoef(a[i])) {
                    k+=a[i];
                    i++;
                }
                i--;

                if (isNum(k)) {
                    num.push(Integer.parseInt(k));
                }else {
                    throw new Exception("Malformed operand Exception :(");
                }
            }
        }

        while(!op.isEmpty()) {
            process(num, op.pop());
        }

        return num.pop();
    }

    private void process(Stack<Integer> stack, char op) throws Exception{

        int l = stack.pop(), r = stack.pop();
        switch(op) {
            case '+':
                stack.push(l + r);
                break;
            case '-':
                stack.push(r - l);
                break;
            case '*':
                stack.push(l*r);
                break;
            case '/':
                stack.push(r/l);
                break;
            default:
                throw new Exception("Operation not found Exception :(");
        }
    }

    private boolean isSpace(char c) {
        return c == ' ';
    }

    private boolean isOperation(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private boolean isKoef(char c) {
        return !isSpace(c) && !isOperation(c) && c != '(' && c != ')';
    }

    private boolean isNum(String c) throws Exception{
        try {
            Integer.parseInt(c);
        }catch(Exception e) {
            return false;
        }
        return true;
    }

    private int priority(char c) {
        if (c == '+' || c == '-') return 1;
        if (c == '*' || c == '/') return 2;
        return -1;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}