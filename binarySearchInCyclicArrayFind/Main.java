import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
    	int []a = new int[]{10, 2, 3, 9};
    	int target = 2;

    	System.out.println(search(a, target));
        out.close();
    }

    public boolean search(int[] a, int target) {
        int n = a.length;
        int lo = 0, hi = n - 1;
        while(lo <= hi) {
            int m = lo + (hi - lo) / 2;
            if (a[m] == target) return true;
            if (a[lo] < a[m]) {
                if (a[lo] <= target && target < a[m]) {
                    hi = m - 1;
                }else {
                    lo = m + 1;
                }
            }else if (a[lo] > a[m]) {
                if (a[m] < target && target <= a[hi]) {
                    lo = m + 1;
                }else {
                    hi = m - 1;
                }
            }else {
                ++lo;
            }
        }
        return false;
    }
    
    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
