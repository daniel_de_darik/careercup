import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	List<Integer> a = new LinkedList<Integer>();
	a.add(1);
	a.add(2);
	a.add(3);
	a.add(4);
	
	
	List<Integer> b = new LinkedList<Integer>();
	b.add(8);
	b.add(9);
	b.add(4);
	b.add(8);
	
	LinkedList<Integer> ret = new LinkedList<Integer>();
	
	int carry = rec(a.iterator(), b.iterator(), ret);
	if (carry > 0) ret.addFirst(carry);
	
	for(int i : ret) out.print(i);
	out.println();
        out.close();
    }
    
    private int rec(Iterator<Integer> a, Iterator<Integer> b, LinkedList<Integer> ret){
	if (!a.hasNext()) return 0;
	Integer f = a.next();
	Integer s = b.next();
	int carry = rec(a, b, ret);
	int t = f + s + carry;
	ret.addFirst(t % 10);
	
	return t/10;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}