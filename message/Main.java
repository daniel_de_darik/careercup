import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        String s = in.readLine();
        String []a = s.split(" ");
        for(int i=0;i<a.length;i++){
            int sum = 0, pow = 1;
            for(int j=0;j<a[i].length();j++){
                sum+=(a[i].charAt(a[i].length()-j-1) - '0') * pow;
                pow*=2;
            }
            System.out.print((char)(sum));
        }
        
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}