import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    int []ans;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	Node root = new Node(20);
	root.left = new Node(8);
	root.left.left = new Node(5);
	root.left.right = new Node(3);
	root.left.right.left = new Node(10);
	root.left.right.right = new Node(14);
	root.right = new Node(22);
	root.right.left = new Node(4);
	root.right.right = new Node(25);
	ans = new int[1000];
	Arrays.fill(ans, -1);
	
	Queue<Node> q = new LinkedList<>();
	root.offset = 500;
	q.add(root);
	while(q.size() > 0){
	    Node cur = q.poll();
	    ans[cur.offset] = cur.val;
	    if (cur.left != null) {
		cur.left.offset = cur.offset - 1;
		q.add(cur.left);
	    }
	    if (cur.right != null){
		cur.right.offset = cur.offset + 1;
		q.add(cur.right);
	    }
	}
	
	for(int i = 0; i < 1000; ++i){
	    if (ans[i] != -1){
		out.print(ans[i] + " ");
	    }
	}
	out.println();	
        out.close();
    }
    
    class Node {
	Node left, right;
	int val, offset;
	
	public Node(int val){
	    this.val = val;
	    this.offset = 0;
	}
	
	public String toString(){
	    return "" + val;
	}
    }

    public static void main(String args[]) throws Exception{
	 new Main().run();
    }
}