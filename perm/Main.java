import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        char []a = next().toCharArray();
        Arrays.sort(a);

        while(a.length > 0) {
            System.out.println(Arrays.toString(a));
            a = nextPermutation(a);
        }

        out.close();	
    }

    private char[] nextPermutation(char []a) {
        int n = a.length;
        if (n == 1) return new char[]{};
        int k = n - 2;
        while(a[k] >= a[k + 1]) {
            k--;
            if (k < 0)
                return new char[]{};
        }

        int idx = -1;
        for(int i=k+1;i<n;i++){
            if (a[k] < a[i] && (idx == -1 || a[idx] > a[i])) {
                idx = i;
            }
        }

        char t = a[idx];
        a[idx] = a[k];
        a[k] = t;

        Arrays.sort(a, k + 1, n);
        
        return a;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}