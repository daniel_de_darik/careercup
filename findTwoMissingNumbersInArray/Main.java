//find two missing numbers in aray
// [1, 3, 4, 6] - two missing are 2, 5
import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        int max = a[0];
        for(int i = 1; i < n; ++i) max = Math.max(max, a[i]);
        
        int set = 0, all = 0;
        for(int i = 0; i < n; ++i) set ^= a[i];
        for(int i = 1; i <= max; ++i) all ^= i;
        set ^= all;
        int lsb = set & ~(set - 1);
        int X = 0, Y = 0;
        for(int i = 1; i <= max; ++i) {
            if ((i & lsb) == 0) {
                X ^= i;
            }else {
                Y ^= i;
            }
        }

        for(int i = 0; i < n; ++i) {
            if ((a[i] & lsb) == 0) {
                X ^= a[i];
            }else {
                Y ^= a[i];
            }
        }

        out.println(X + " " + Y);
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}