//implementation of queue, which supports max in queue O(1)
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        int n = nextInt();
        MyQueue q = new MyQueue();
        for(int i = 0; i < n; ++i) {
            String o = next();
            if (o.equals("ADD")) {
                q.add(nextInt());
            }else if (o.equals("POP")){
                q.poll();
            }else {
                out.println(q.max());
            }
        }

        out.close();
    }

    interface P {
        public int getA();
        public int getB();
    }

    class Pair implements P {
        int a, b;
        public Pair(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public int getA() {
            return this.a;
        }

        public int getB() {
            return this.b;
        }
    }

    final static int oo = Integer.MAX_VALUE / 2;

    class MyQueue {
        Stack<P> s1, s2;
        public MyQueue() {
            s1 = new Stack<>();
            s2 = new Stack<>();
        }

        public int max() {
            if (s1.size() == 0 || s2.size() == 0) {
                if (s1.size() == 0 && s2.size() == 0) return -oo;
                if (s1.size() == 0) return s2.peek().getB();
                return s1.peek().getB();
            }
            return Math.max(s1.peek().getB(), s2.peek().getB());
        }

        public void add(int x) {
            if (s1.size() > 0) {
                P p = s1.peek();
                s1.add(new Pair(x, Math.max(x, p.getB())));
            }else {
                s1.add(new Pair(x, x));
            }
        }

        public void poll() {
            if (s2.size() == 0) {
                while(s1.size() > 0) {
                    P top = s1.pop();
                    int max = -1;
                    if (s2.size() > 0) {
                        max = Math.max(top.getA(), s2.peek().getB());
                    }else {
                        max = top.getA();
                    }
                    s2.add(new Pair(top.getA(), max));
                }
            }
            if (s2.size() > 0) s2.pop();
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
