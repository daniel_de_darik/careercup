import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    int n, m;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	n = 9;
	m = 9;
	char [][]a = new char[n][m];
	for(int i = 0; i < n; ++i){
	    a[i] = next().toCharArray();
	}

	out.println(validSudoku(a));
        out.close();
    }

    public boolean validSudoku(char [][]a){
	n = a.length;
        if (n == 0) return true;
        m = a[0].length;
        for(int i = 0; i < n; ++i){
            int mask = 0;
            for(int j = 0; j < m; ++j){
                if (a[i][j] == '.') continue;
                int x = a[i][j] - '0';
                if ((mask & (1 << x)) > 0){
                    return false;
                }
                mask ^= 1 << x;
            }
        }
        for(int j = 0; j < m; ++j){
            int mask = 0;
            for(int i = 0; i < n; ++i){
                if (a[i][j] == '.') continue;
                int x = a[i][j] - '0';
                if ((mask & (1 << x)) > 0){
                    return false;
                }
                mask ^= 1 << x;
            }
        }
        
        for(int i = 0; i < n / 3; i++){
            for(int j = 0; j < m / 3; j++){
                int mask = 0;
                for(int r = i * 3; r < (i + 1) * 3; ++r){
                    for(int c = j * 3; c < (j + 1) * 3; ++c){
                        if (a[r][c] == '.') continue;
                        int x = a[r][c] - '0';
                        if ((mask & (1 << x)) > 0){
                            return false;
                        }
                        mask ^= 1 << x;
                    }
                }
            }
        }
        
        return true;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
