import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();
        int num = a[0], cnt = 1;
        for(int i=1;i<n;i++){
            if (a[i] == num) cnt++;
            else {
                if (num == -1) {
                    num = a[i];
                    cnt = 1;
                }else {
                    if (cnt > 0) cnt--;
                    if (cnt == 0) {
                        num = -1;
                    }
                }
            }
        }
        out.println(num + " " + find(a));
        out.close();	
    }

    private int find(int []a) {
        int n = a.length;
        int cnt[] = new int[32];
        for(int i=0;i<32;i++){
            for(int j=0;j<n;j++){
                cnt[i]+=((a[j] >> i) & 1);
            }
        }
        int ret = 0;
        for(int i=0;i<32;i++){
            if (cnt[i] > n/2) ret|=1 << i;
        }
        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}