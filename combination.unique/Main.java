/*
If given a number find a number if it is colorful. A number is said to be colorful if all its possible unique permutations multiplication result different. 
Eg: if n = 1234 then permutations are (1,2),(1,3),(1,4), (2,3),(2,4),(3,4),(1,2,3), (1,2,4), (2,3,4). That's it, no other combination. Find the multiplication of digits in each combination and if any of them repeats then number is not colorful.
*/
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        char []a = next().toCharArray();
        int n = a.length;
        if (n <= 2) {
            System.out.println("colorful");
            return;
        }
        int []c = new int[10];
        for(int i = 0; i < n; ++i) c[a[i] - '0']++;
        int size = 0;
        boolean hasMoreThanOne = false;
        for(int i = 0; i < 10; ++i) {
            if (c[i] > 0) ++size;
            if (c[i] > 1) hasMoreThanOne = true;
        }
        
        if (size == 1) {
            System.out.println("not colorful");
            return;
        }
        if (hasMoreThanOne) {
            System.out.println("not colorful");
            return;
        }
        int []set = new int[400000];
        for(int mask = 0; mask < (1<<n); ++mask) {
            if (Integer.bitCount(mask) <= 1) continue;
            int prod = 1;
            for(int i = 0; i < n; ++i) {
                if ((mask & (1 << i)) > 0) {
                    prod *= (a[i] - '0');
                }
            }
            if (set[prod] > 0) {
                System.out.println("not colorful");
                return;
            }
            ++set[prod];
        }
        System.out.println("colorful");
        out.close();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
