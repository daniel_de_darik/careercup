import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    Node root;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	add(50);
	add(25);
	add(15);
	add(18);
	add(2);
	add(30);
	add(75);
	add(70);
	add(85);
	add(28);
	add(35);
	add(29);
	add(33);
	
	print(root);
	System.out.println();
	
	remove(28);
	print(root);
	System.out.println();
	
	Random rnd = new Random();
	for(int i=0;i<20;i++){
	    int x = rnd.nextInt(86);
	    System.out.println(x + " " + find(x));
	}
	
        out.close();
    }
    
    private void remove(int key){
	Node cur = root, parent = root;
	boolean isLeft = false;
	while(cur.key != key){
	    parent = cur;
	    if (cur.key < key){
		isLeft = false;
		cur = cur.right;
	    }else{
		isLeft = true;
		cur = cur.left;
	    }
	    if (cur == null) return;
	}
	
	if (cur.left == null && cur.right == null){
	    if (isLeft) parent.left = null;
	    else parent.right = null;
	}else if (cur.left == null){
	    if (isLeft) parent.left = cur.right;
	    else parent.right = cur.right;
	}else if (cur.right == null){
	    if (isLeft) parent.left = cur.left;
	    else parent.right = cur.left;
	}else{
	    Node focus = cur.right, p = null;
	    while(focus.left != null){
		p = focus;
		focus = focus.left;
	    }
	    cur.key = focus.key;
	    if (p == null){
		cur.right = focus.right;
	    }else{
		p.left = focus.right;
	    }
	}
    }
    
    private void print(Node node){
	if (node == null) return;
	print(node.left);
	System.out.print(node.key + " ");
	print(node.right);
    }
    
    private void add(int key){
	Node node = new Node(key);
	if (root == null){
	    root = node;
	    return;
	}
	Node cur = root, parent = root;
	boolean isLeft = false;
	while(true){
	    parent = cur;
	    if (cur.key < key){
		cur = cur.right;
		isLeft = false;
	    }else{
		cur = cur.left;
		isLeft = true;
	    }
	    if (cur == null){
		if (isLeft) parent.left = node;
		else parent.right = node;
		break;
	    }
	}
    }
    
    private boolean find(int key){
	Node cur = root;
	while(cur != null){
	    if (cur.key < key){
		cur = cur.right;
	    }else if (cur.key > key){
		cur = cur.left;
	    }else{
		return true;
	    }
	}
	
	return false;
    }
    
    static class Node{
	int key;
	Node left, right;
	
	public Node(int key){
	    this.key = key;
	}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}