import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        ListNode head = new ListNode(1);
        ListNode runner = head;
        runner.next = new ListNode(2);
        runner = runner.next;
        runner.next = new ListNode(3);
        runner = runner.next;
        runner.next = new ListNode(4);
        runner = runner.next;
        runner.next = new ListNode(5);

        // print(head);

        reorderList(head);

        while(head != null) {
            System.out.println(head);
            head = head.next;
        }

        out.close();
    }

    public void reorderList(ListNode head) {
        int size = sizeOf(head);
        if (size <= 2) return;
        int x = (size + 1)  / 2;
        ListNode head2 = head, prev = null;
        while(x > 0){
            --x;
            prev = head2;
            head2 = head2.next;
        }
        if (prev != null) prev.next = null;
        head2 = reverse(head2);
        
        ListNode runner1 = head, runner2 = head2;
        while(runner1 != null && runner2 != null){
            ListNode next1 = runner1.next, next2 = runner2.next;
            runner1.next = runner2;
            runner2.next = next1;
            runner1 = next1;
            runner2 = next2;
        }
    }
    
    private ListNode reverse(ListNode runner){
        // if (runner == null) return null;
        // ListNode nhead = reverse(runner.next);
        // if (nhead == null) return runner;
        // runner.next.next = runner;
        // runner.next = null;
        // return nhead;
        ListNode prev = null;
        while(runner != null) {
            ListNode next = runner.next;
            runner.next = prev;
            prev = runner;
            runner = next;
        }
        return prev;
    }
    
    private int sizeOf(ListNode head){
        ListNode runner = head;
        int size = 0;
        while(runner != null){
            ++size;
            runner = runner.next;
        }
        return size;
    }

    private void print(ListNode head) {
        ListNode runner = head;
        while(runner != null) {
            System.out.println(runner);
            runner = runner.next;
        }
        System.out.println();
    }

    class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }

        public String toString(){
            return ""+this.val;
        }
    }
 
    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
