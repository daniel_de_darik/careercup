import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    char [][]a;
    int n,m, ans;
    String req;
    
    int di[] = new int[]{-1, 1, 0, 0};
    int dj[] = new int[]{0, 0, 1, -1};
    
    final static int BASE = 20;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
	n = nextInt();
	m = nextInt();
	a = new char[n][m];
	for(int i=0;i<n;i++) a[i] = next().toCharArray();
	
	req = next();
	
	ans = 0;
	
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
		String s = Character.toString(a[i][j]);
		HashSet<Integer> path = new HashSet<Integer>();
		go(i, j, s, path, req);
	    }
	}
	
	out.println(ans);
        out.close();
    }
    
    private void go(int i, int j, String s, HashSet<Integer> path, String req){
	if (s.length() == req.length()){
	    if (s.equals(req)) ans++;
	    return;
	}
	
	for(int k=0;k<4;k++){
	    int ni = i + di[k];
	    int nj = j + dj[k];
	    if (!isOk(ni, nj)) continue;
	    int x = i * BASE + j;
	    if (path.contains(x)) continue;
	    HashSet<Integer> copy = (HashSet<Integer>)path.clone();
	    copy.add(x);
	    go(ni, nj, s + a[ni][nj], copy, req);
	}
    }
    
    private boolean isOk(int x, int y){
	if (x < 0 || x >= n) return false;
	if (y < 0 || y >= m) return false;
	return true;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}