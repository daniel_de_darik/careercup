/*
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.

Note:
You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
*/
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n];
        for(int i = 0; i < n; ++i) a[i] = nextInt();
        System.out.println(maxProfit(a));
        out.close();
    }

    final int oo = Integer.MAX_VALUE / 2;
    
    public int maxProfit(int[] a) {
        if (a.length == 0) return 0;
        int min = a[0], n = a.length;
        int []left = new int[n];
        Arrays.fill(left, -oo);
        left[0] = 0;
        for(int i = 1; i < n; ++i) {
            left[i] = Math.max(left[i-1], a[i] - min);
            min = Math.min(min, a[i]);
        }
        
        int []right = new int[n];
        Arrays.fill(right, -oo);
        right[n-1] = 0;
        int max = a[n-1];
        for(int i = n - 2; i >= 0; --i){
            right[i] = Math.max(right[i+1], max - a[i]);
            max = Math.max(max, a[i]);
        }

        // System.out.println(Arrays.toString(left));
        // System.out.println(Arrays.toString(right));

        int ret = -oo;
        for(int i = 0; i < n; ++i){
            ret = Math.max(ret, left[i] + ((i + 1 < n) ? right[i+1] : 0));
        }
        return ret;
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
