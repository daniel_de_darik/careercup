import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        HashTable ht = new HashTable(2);
        ht.add("darik", 1);
        ht.add("almas", 2);
        ht.add("ololoshka", 3);

        System.out.println("ololoshka = " + ht.get("ololoshka"));
        ht.remove("ololoshka");
        System.out.println("ololoshka has been removed");
        System.out.println("ololoshka = " + ht.get("ololoshka"));
        System.out.println("almas = " + ht.get("almas"));
        System.out.println("someone = " + ht.get("someone"));
        System.out.println("darik = " + ht.get("darik"));

        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}

class HashTable {
    int elm, MOD, size;
    String []a;
    int value[];
    public HashTable(int size) {
        this.elm = 0;
        this.size = nextPrime(size);
        a = new String[this.size];
        value = new int[this.size];
        Arrays.fill(a, "-1");
        MOD = 1000000007;
    }

    public void add(String word, int val) {
        // System.out.println(word + " " + val);
        if (elm > size/2) {
            System.out.println("resizing...");
            resize(size * 2);
        }
        int hash = hashFunction(word);
        int index = hash;
        index%=size;
        while(a[index] != "-1") {
            index = getNext(index, hash);
            index%=size;
        }

        a[index] = word;
        value[index] = val;
        elm++;
    }

    public int get(String word) {
        int hash = hashFunction(word);
        int index = hash;
        index%=size;

        while(a[index] != "-1") {
            if (a[index] == word)
                return value[index];
            index = getNext(index, hash);
            index%=size;
        }

        return Integer.MAX_VALUE;
    }

    public boolean remove(String word) {
        int hash = hashFunction(word);
        int index = hash;
        index%=size;
        while(a[index] != "-1") {
            if (a[index] == word) {
                elm--;
                a[index] = "-1";
                return true;
            }
            index = getNext(index, hash);
            index%=size;
        }

        return false;
    }

    private void resize(int newsize) {
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<Integer> vals = new ArrayList<Integer>();

        for(int i=0;i<size;i++){
            if (a[i] != "-1") {
                words.add(a[i]);
                vals.add(value[i]);
            }
        }

        size = nextPrime(newsize);
        a = new String[size];
        Arrays.fill(a, "-1");
        value = new int[size];
        elm = 0;
        for(int i=0;i<words.size();i++){
            add(words.get(i), vals.get(i));
        }
    }


    private int nextPrime(int x) {
        for(int i=x;;i++)
            if (isPrime(i))
                return i;
    }

    private boolean isPrime(int x) {
        if (x % 2 == 0)
            return false;
        for(int i=3;i*i<=x;i+=2)
            if (x % i == 0)
                return false;
        return true;
    }

    private int hashFunction(String s) {
        long hash = 0;
        long p = 1;
        long mul = 27;
        for(int i=0;i<s.length();i++){
            int x = s.charAt(i) - 'a' + 1;
            x*=p;
            x%=MOD;
            hash+=x;
            hash%=MOD;
            p*=mul;
            p%=MOD;
        }

        return (int)hash;
    }

    private int getNext(int x, int hash) {
        return x + (7 - hash % 7);
    }
}