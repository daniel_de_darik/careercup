import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        ListNode node = new ListNode(2);
        node.next = new ListNode(1);
        // System.out.println(simplifyPath(next()));

        ListNode ret = partition(node, 2);
        while(ret != null) {
            System.out.println(ret);
            ret = ret.next;
        }
        out.close();
    }

    public ListNode partition(ListNode head, int x) {
        ListNode tail = null, cur = head, HEAD = null;
        while(cur != null){
            if (cur.val < x){
                if (tail == null) {
                    tail = cur;
                    HEAD = tail;
                }else {
                    tail.next = cur;
                    tail = tail.next;
                    tail.next = null;
                }
            }
            cur = cur.next;
        }
        // cur = head;
        // while(cur != null) {
        //     System.out.println(cur);
        //     cur = cur.next;
        // }
        cur = head;
        // System.out.println(cur);
        // System.out.println(cur.next);
        // System.out.println(cur.next.next);
        while(cur != null){
            if (cur.val >= x){
                if (tail == null) {
                    tail = cur;
                    HEAD = tail;
                }else {
                    tail.next = cur;
                    tail = tail.next;
                    tail.next = null;
                }
            }
            cur = cur.next;
            // System.out.println(cur);
        }
        
        return HEAD;
    }

    public String simplifyPath(String path) {
        String []a = path.split("/");
        System.out.println(Arrays.toString(a));
        Stack<String> stack = new Stack<String>();
        for(String i : a){
            if (i.equals("") || i.equals(".")) continue;
            if (i.equals("..")){
                if (stack.size() > 0) stack.pop();
            } 
            else stack.add(i);
        }
        if (stack.size() == 0) return "/";
        List<String> lst = new LinkedList<String>();
        while(stack.size() > 0) lst.add(stack.pop());
        Collections.reverse(lst);
        String ret = "";
        for(String i : lst) ret += "/" + i;
        
        return ret;
    }

    public class ListNode {
        int val;
        ListNode next;
        public ListNode(int x) {
           val = x;
           next = null;
        }

        public String toString() {
            return Integer.toString(val);
        }
    }
 

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
