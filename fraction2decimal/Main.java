import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt(), m = nextInt();
        System.out.println(fractionToDecimal(n, m));
        out.close();
    }

    public String fractionToDecimal(int numerator, int denominator) {
        long num = numerator, den = denominator;
        StringBuilder sb = new StringBuilder();
        boolean positive = false;
        if ((num >= 0 && den >= 0) || (num <= 0 && den <= 0)) positive = true;
        if (!positive) sb.append("-");
        num = Math.abs(num);
        den = Math.abs(den);
        sb.append(num / den);
        num %= den;
        if (num == 0) return sb.toString();
        sb.append(".");
        
        Map<Long, Integer> map = new HashMap<Long, Integer>();
        map.put(num, sb.length());
        while(num != 0){
            num *= 10;
            sb.append(num / den);
            num %= den;
            if (map.containsKey(num)){
                int pos = map.get(num);
                sb.insert(pos, '(');
                sb.append(")");
                
                return sb.toString();
            }
            map.put(num, sb.length());
            
        }
        return sb.toString();
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
