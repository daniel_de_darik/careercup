import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt();
	out.println(square(n) + " " + n * n);
        out.close();
    }
    
    private long square(long n){
	long ret = 0, x = n;
	while(x > 0){
	    if ((x & 1) > 0) ret+=n;
	    x>>=1;
	    n<<=1;
	}
	return ret;
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}