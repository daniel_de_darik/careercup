import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
		int n = nextInt();
		P a[] = new P[n];
		for(int i=0;i<n;i++) a[i] = new P(nextInt(), nextInt());
		
		Arrays.sort(a, 0, n, new cmpX());
		TreeSet<P> set = new TreeSet<P>(new cmpY());
		set.add(a[0]);
		
		double h = Double.MAX_VALUE/2;
		
		for(int i=1, yk = 0;i<n;i++){
		    while(a[i].x - a[yk].x > h){
				set.remove(a[yk]);
				yk++;
		    }
		    
		    double y = a[i].y - h;
		    while(true){
				P p = set.higher(new P(100000, y));
				if (p == null) break;
				double dis = dis(p, a[i]);
				if (dis < h) h = dis;
				y = p.y;
		    }
		    set.add(a[i]);
		}
	
		out.println(h);
        out.close();
    }
    
    private double dis(P a, P b){
		return Math.hypot(a.x - b.x, a.y - b.y);
    }
    
    static class P{
		double x, y;
		public P(double x,double y){
		    this.x = x;
		    this.y = y;
		}
    }
    
    static class cmpX implements Comparator<P>{
		public int compare(P a, P b){
		    if (a.x > b.x) return 1;
		    if (a.x < b.x) return -1;
		    if (a.y > b.y) return 1;
		    if (a.y < b.y) return -1;
		    return 0;
		}
    }
    
    static class cmpY implements Comparator<P>{
		public int compare(P a, P b){
		    if (a.y > b.y) return 1;
		    if (a.y < b.y) return -1;
		    if (a.x > b.x) return 1;
		    if (a.x < b.x) return -1;
		    return 0;
		}
    }

    public static void main(String args[]) throws Exception{
		new Solver().run();
    }
}