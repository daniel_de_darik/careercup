import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
		return Integer.parseInt(next());
    }
    
    double h = Double.MAX_VALUE/2;
    int n;
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	
		n = nextInt();
		P a[] = new P[n];
		for(int i=0;i<n;i++){
		    a[i] = new P(nextInt(), nextInt());
		}
		
		Arrays.sort(a, 0, n, new cmpX());
		find(0, n-1, a);
		
		out.println(h);
        out.close();
    }
    
    private void find(int l, int r, P []a){
		if (r - l <= 3){
		    for(int i=l;i<=r;i++){
				for(int j=i+1;j<=r;j++){
				    updateAns(a[i], a[j]);
				}
		    }
		    Arrays.sort(a, l, r + 1, new cmpY());
		    return;
		}
		
		int m = (l + r) >> 1;
		int x = a[m].x;
		find(l, m, a);
		find(m+1, r, a);
		Arrays.sort(a, l, r+1, new cmpY());
		
		P buffer[] = new P[n];
		int sz = 0;
		
		for(int i=l;i<=r;i++){
		    if (Math.abs(x - a[i].x) < h){
				for(int j=sz-1;j>=0 && a[i].y - buffer[j].y < h;j--){
				    updateAns(a[i], buffer[j]);
				}
				buffer[sz++] = a[i];
		    }
		}
    }
    
    private void updateAns(P a, P b){
		double dis = a.dis(b);
		if (h > dis) h = dis;
    }
    
    static class P{
		int x, y;
		public P(int x, int y){
		    this.x = x;
		    this.y = y;
		}
		
		public double dis(P b){
		    return Math.hypot(x - b.x, y - b.y);
		}
    }
    
    static class cmpX implements Comparator<P>{
		public int compare(P a, P b){
		    return a.x - b.x;
		}
    }
    
    static class cmpY implements Comparator<P>{
		public int compare(P a, P b){
		    return a.y - b.y;
		}
    }

    public static void main(String args[]) throws Exception{
	new Main().run();
    }
}