import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    Node root1, root2;

    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        root1 = new Node(10);
        // add(10, root1);
        add(8, root1);
        add(7, root1);
        add(1, root1);
        add(6, root1);
        add(5, root1);
        add(9, root1);
        add(14, root1);
        add(13, root1);
        add(12, root1);
        add(20, root1);
        add(15, root1);
        
        root2 = new Node(13);
        // add(13, root2);
        add(9, root2);
        add(7, root2);
        add(6, root2);
        add(10, root2);
        add(12, root2);
        add(15, root2);
        add(20, root2);
        add(22, root2);
        add(21, root2);

        Stack<Node> st1 = new Stack<Node>();
        Stack<Node> st2 = new Stack<Node>();

        Node cur = root1;
        while(cur != null) {
            st1.push(cur);
            cur = cur.left;
        }

        cur = root2;
        while(cur != null) {
            st2.push(cur);
            cur = cur.left;
        }

        while(st1.size() > 0 && st2.size() > 0) {
            Node x = st1.peek(), y = st2.peek();
            if (x.key <= y.key) {
                System.out.print(x + " ");
                st1.pop();
                x = x.right;
                while(x != null) {
                    st1.push(x);
                    x = x.left;
                }
            }else {
                st2.pop();
                System.out.print(y + " ");
                y = y.right;
                while(y != null) {
                    st2.push(y);
                    y = y.left;
                }
            }
        }

        while(st1.size() > 0) {
            Node x = st1.pop();
            System.out.print(x + " ");
            x = x.right;
            while(x != null) {
                st1.push(x);
                x = x.left;
            }
        }
        while(st2.size() > 0) {
            Node x = st2.pop();
            System.out.print(x + " ");
            x = x.right;
            while(x != null) {
                st2.push(x);
                x = x.left;
            }
        }

        System.out.println();

        int res = dfs(root1, 7, 12);
        System.out.println(res);

        for(int i=0;i<15;i++){
            System.out.println(getRandom(root1));
        }

        out.close();	
    }

    private Node getRandom(Node root) {

        Node ret = null;

        Queue<Node> q = new ArrayDeque<Node>();
        q.add(root);
        int cnt = 1;
        while(q.size() > 0) {
            Node cur = q.poll();
            if (1./cnt >= Math.random()) {
                ret = cur;
            }
            if (cur.left != null) {
                q.add(cur.left);
            }
            if (cur.right != null) {
                q.add(cur.right);
            }
            cnt++;
        }

        return ret;
    }

    private int dfs(Node root, int min, int max){ 
        if (root == null) return 0;
        int res = 0;
        if (min <= root.key && root.key <= max) {
            res = root.key + dfs(root.left, min, max) + dfs(root.right, min, max);
        }else if (min > root.key) {
            res = dfs(root.right, min, max);
        }else {
            res = dfs(root.left, min, max);
        }
        return res;
    }

    private void add(int x, Node root) {
        Node elm = new Node(x);
        if (root == null) {
            root = elm;
            return;
        }

        Node cur = root, parent = null;
        boolean isLeft = false;
        while(true) {
            parent = cur;
            if (cur.key < elm.key) {
                isLeft = false;
                cur = cur.right;
            }else {
                isLeft = true;
                cur = cur.left;
            }
            if (cur == null) {
                if (isLeft) parent.left = elm;
                else parent.right = elm;
                return;
            }
        }
    }

    static class Node{
        int key;
        Node left, right;
        public Node(int key){
            this.key = key;
        }

        @Override
        public String toString() {
            return Integer.toString(key);
        }
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}