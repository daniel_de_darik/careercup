import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
    	int n = nextInt();
    	Node []a = new Node[n];
    	for(int i=0;i<n;i++) a[i] = new Node(nextInt(), i);

    	Stack<Node> stack = new Stack<Node>();
    	stack.add(a[0]);
    	Node root = a[0];
    	Node max = root;
    	for(int i=1;i<n;i++){
    		if (a[i-1].value >= a[i].value) {
    			a[i-1].left = a[i];
    		}else{
    			Node prev = null;
    			while(stack.size() > 0 && stack.peek().value < a[i].value) {
    				prev = stack.pop();
    			}
    			prev.right = a[i];
    		}
    		stack.add(a[i]);
    	}
    	print(a[0]);
        out.close();	
    }

    private void print(Node cur) {
    	if (cur == null) return;
    	out.println(cur.value);
    	print(cur.left);
    	print(cur.right);
    }

    static class Node{
    	Node left, right;
    	int value, idx;
    	public Node(int value, int idx) {
    		this.value = value;
    		this.idx = idx;
    	}
    }


    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}