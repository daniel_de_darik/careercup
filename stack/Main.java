import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);

        System.out.println("Here is my Stack");

        MyStack stack = new MyStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        while(stack.size() > 0) {
            System.out.println(stack.pop());
        }

        System.out.println("Here is my Queue");
        MyQueue q = new MyQueue();
        q.add(1);
        q.add(2);
        q.add(3);
        q.add(4);

        while(q.size() > 0) {
            System.out.println(q.poll());
        }

        System.out.println("Here is my SetOfStack");
        SetOfStack st = new SetOfStack(2);
        st.add(1);
        System.out.println(st.setSize());
        st.add(2);
        System.out.println(st.setSize());
        st.add(3);
        System.out.println(st.setSize());
        st.add(4);
        System.out.println(st.setSize());
        st.add(5);
        System.out.println(st.setSize());
        st.add(6);
        System.out.println(st.setSize());
        st.add(7);
        System.out.println(st.setSize());

        while(st.size() > 0) {
            System.out.println(st.setSize() + " " + st.pop());
        }

        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}

class Node {
    public int data;
    public Node next;

    public Node(int data) {
        this.data = data;
        this.next = next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}

class MyStack{
    Node top;
    int size = 0;

    public void push(int x){
        Node t = new Node(x);
        t.setNext(top);
        top = t;
        size++;
    }

    public int pop() throws Exception{
        if (size() == 0)
            throw new Exception("No data in stack :(");

        size--;
        Node item = top;
        top = top.next;

        return item.data;
    }

    public int peek() throws Exception{
        if (size() == 0)
            throw new Exception("No data in stack :(");
        return top.data;
    }

    public int size() {
        return this.size;
    }
}

class MyQueue {
    Node f,l;
    int size = 0;

    public void add(int x) {
        Node t = new Node(x);
        if (size() == 0) {
            f = t;
        }else{
            l.next = t;
        }
        l = t;
        size++;
    }

    public int poll() throws Exception{
        if (size() == 0)
            throw new Exception("No data in Queue :(");

        Node item = f;
        f = f.next;

        size--;

        return item.data;
    }

    public int size() {
        return this.size;
    }
}

class SetOfStack {

    ArrayList<int[]> l;
    public int capacity, curStack, yk;

    public SetOfStack(int capacity) {
        this.capacity = capacity;
        curStack = 0;
        yk = 0;
        l = new ArrayList<int[]>();
        int st[] = new int[capacity];
        l.add(st);
    }

    public void add(int x) {
        if (yk + 1 <= capacity) {
            int st[] = l.get(curStack);
            st[yk++] = x;
        }else {
            yk = 0;
            int []st = new int[capacity];
            st[yk++] = x;
            curStack++;
            l.add(st);
        }
    }

    public int pop() throws Exception{
        if(curStack + yk == 0) 
            throw new Exception("No data in Stack");

        int []st = l.get(curStack);
        int ret = st[--yk];
        
        if (yk == 0) {
            curStack--;
            if (curStack >= 0) {
                yk = capacity;
                l.remove(curStack+1);
            }else{
                curStack = 0;
            }
        }

        return ret;
    }

    public int size() {
        return capacity * curStack + yk;
    }

    public int setSize() {
        return l.size();
    }
}