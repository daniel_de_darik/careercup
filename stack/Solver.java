import java.util.*;

public class Solver{

    public void run(){
        MyQueue q = new MyQueue();
        q.push(1);
        q.push(2);
        q.pop();
        q.push(3);
        q.push(4);
        q.pop();
        q.push(5);

        while(q.size() > 0){
            System.out.println(q.pop() + " has been poped from queue");
}
}
    
    public static void main(String args[]){
        new Solver().run();
}
}

class MyQueue{
    Stack<Integer> s1, s2;
    public MyQueue(){
        s1 = new Stack<Integer>();
        s2 = new Stack<Integer>();
}

public int size(){
    return s1.size() + s2.size();
}

public void push(int x){
    s1.push(x);
}

    public int pop(){
        if (s2.size() == 0)
            this.transfer();
        return s2.pop();
}

public int peek(){
if (s2.size() == 0)
this.transfer();
return s2.peek();    
}

private void transfer(){
    while(s1.size() > 0)
        s2.add(s1.pop());
}
}
