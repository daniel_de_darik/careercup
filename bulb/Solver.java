import java.util.*;
import java.io.*;

public class Solver{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	while (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	return Integer.parseInt(next());
    }
    
    int dp[][];
    final static int oo = Integer.MAX_VALUE;
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
	int n = nextInt(), k = nextInt();
	
	dp = new int[n+1][k+1];
	for(int i=0;i<=n;i++){
	    Arrays.fill(dp[i], -1);
	}
	
	int ans = go(n, k);
	
	// for(int i=0;i<=n;i++){
	//     for(int j=0;j<=k;j++){
	// 	System.out.print(((dp[i][j]==-1)?0:dp[i][j]) + " ");
	//     }
	//     System.out.println();
	// }
	
	out.println(ans);
        out.close();
    }
    /*
    private int go2(int n, int k){
	if (n <= 0) return 0;
	if (n == 1){
	    if (k > 0) return dp[n][k] = 1;
	    return oo;
	}
	if (k == 1) return dp[n][k] = n;
	if (k == 0) return oo;
	
	if (dp[n][k] != -1) return dp[n][k];
	
	int l = -1, r = n+1;
	//System.out.println("n="+n+" k="+k);
	while(r - l > 2){
	    int m1 = l + (r-l)/3;
	    int m2 = r - (r-l)/3;
	    
	    int f1 = Math.max(go2(m1-1, k-1), go2(n-m1, k));
	    int f2 = Math.max(go2(m2-1, k-1), go2(n-m2, k));
	    //System.out.println("m1="+m1 + " f1=" + f1 + " m2=" + m2 + " f2=" + f2);
	    if (f1 < f2){
		r = m2;
	    }else{
		l = m1;
	    }
	}
	
	int ret = oo;
	for(int i=l;i<=r;i++){
	    int t = Math.max(go2(i-1, k-1), go2(n-i, k)) + 1;
	    ret = Math.min(ret, t);
	}
	//System.out.println(n + " " + k + " " + ret);
	return dp[n][k] = ret;
    }
    */
    private int go(int n, int k){
	if (n <= 0) return 0;
	if (n == 1){
	    if(k > 0) return dp[n][k] = 1;
	    return oo;
	}
	if (k == 1) return dp[n][k] = n;
	if (k == 0) return oo;
	
	if (dp[n][k] != -1) return dp[n][k];
	int ret = oo;
	for(int i=1;i<=n;i++){
	    int t = Math.max(go(i-1, k-1), go(n-i, k)) + 1;
	    ret = Math.min(ret, t);
	}
	
	return dp[n][k] = ret;
    }

    public static void main(String args[]) throws Exception{
	new Solver().run();
    }
}