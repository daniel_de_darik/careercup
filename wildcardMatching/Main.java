/*
'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).

The matching should cover the entire input string (not partial).

The function prototype should be:
bool isMatch(const char *s, const char *p)

Some examples:
isMatch("aa","a") → false
isMatch("aa","aa") → true
isMatch("aaa","aa") → false
isMatch("aa", "*") → true
isMatch("aa", "a*") → true
isMatch("ab", "?*") → true
isMatch("aab", "c*a*b") → false
*/
import java.util.*;
import java.io.*;
	
public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
	
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
	
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        String s = next(), p = next();
        System.out.println(isMatch(s, p));
        out.close();
    }

    public boolean isMatch(String s, String p) {
        int ats = 0, atp = 0, match = -1, star = -1;
        while(ats < s.length()) {
            if (atp < p.length() && (s.charAt(ats) == p.charAt(atp) || p.charAt(atp) == '?')) {
                ++atp;
                ++ats;
            }else if (atp < p.length() && p.charAt(atp) == '*') {
                star = atp;
                match = ats;
                ++atp;
            }else if (star != -1) {
                atp = star + 1;
                ++match;
                ats = match;
            }else return false;
        }

        while(atp < p.length() && p.charAt(atp) == '*') {
            ++atp;
        }
        return atp == p.length();
    }

    public boolean isMatch2(String s, String p) {
        int n = s.length(), m = p.length();
        boolean [][]dp = new boolean[2][m + 1];
        dp[0][0] = true;
        int x = 0;
        for(int i = 0; i <= n; ++i) {
            // Arrays.fill(dp[x], false);
            for(int j = 1; j <= m; ++j) {
                if (i == 0) {
                    if (p.charAt(j - 1) == '*') {
                        // dp[i][j] = dp[i][j-1];
                        dp[x][j] = dp[x][j-1];
                    }
                }else {
                    if (s.charAt(i - 1) == p.charAt(j - 1) || p.charAt(j - 1) == '?') {
                        // dp[i][j] = dp[i - 1][j-1];
                        dp[x][j] = dp[1 - x][j - 1];
                    }else {
                        if (p.charAt(j - 1) == '*') {
                            // dp[i][j] = dp[i][j-1] || dp[i - 1][j];
                            dp[x][j] = dp[x][j-1] || dp[1 - x][j];
                        }
                    }
                }
            }
            // System.out.println(i + " " + Arrays.toString(dp[x]));
            x = 1 - x;
        }

        // for(int i = 0; i <= n; ++i) {
        //     System.out.println(Arrays.toString(dp[i]));
        // }
        return dp[1 - x][m];
        // return dp[n][m];
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}
