import java.util.*;
import java.io.*;

public class Main{

    BufferedReader in;
    StringTokenizer str = null;
    PrintWriter out;

    private String next() throws Exception{
    	if (str == null || !str.hasMoreElements())
    	    str = new StringTokenizer(in.readLine());
    	return str.nextToken();
    }
    
    private int nextInt() throws Exception{
	   return Integer.parseInt(next());
    }
    
    public void run() throws Exception{
    	in = new BufferedReader(new InputStreamReader(System.in));
    	out = new PrintWriter(System.out);
        int n = nextInt();
        int []a = new int[n];
        for(int i=0;i<n;i++) a[i] = nextInt();
        int min = a[0], max = a[0];
        for(int i=1;i<n;i++){
            min = Math.min(a[i], min);
            max = Math.max(a[i], max);
        }
        if (max - min + 1 > n) {
            System.out.println("NO");
            return;
        }
        for(int i=0;i<n;i++) a[i]-=min;
        for(int i=0;i<n;i++){
            if (a[i] == -1) continue;
            if (a[i] == a[a[i]]) {
                if (i != a[i]) a[i] = -1;
            }else {
                int j = a[i];
                int t = a[i];
                a[i] = a[j];
                a[j] = t;
                i--;
            }
        }
        // System.out.println(Arrays.toString(a));
        int last = -1;
        for(int i=0;i<n;i++) if (a[i] != -1) last = i;
        for(int i=0;i<last-1;i++){
            if (a[i] + 1 != a[i+1]) {
                System.out.println("NO");
                return;
            }
        }
        System.out.println("YES");
        out.close();	
    }

    public static void main(String args[]) throws Exception{
	   new Main().run();
    }
}